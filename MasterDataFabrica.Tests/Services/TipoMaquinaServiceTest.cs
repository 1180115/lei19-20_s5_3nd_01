using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Domain.Services;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Tests.Services
{
    public class TipoMaquinaServiceTest
    {
        private readonly Mock<ITipoMaquinaRepository> _tipoMaquinaRepository;
        private readonly Mock<IOperacaoRepository> _operacaoRepository;
        private readonly Mock<IOperacoesTiposMaquinaRepository> _operacoesTipoMaquinaRepository;

        private TipoMaquinaService _tipoMaquinaService;

        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();
        private readonly static Operacao fakeOperacao = new Operacao("Descricao", new Duracao(3));
        private readonly static TipoMaquina fakeTipoMaquina = new TipoMaquina("Nome");
        private readonly static OperacoesTiposMaquina fakeOperacaoTipoMaquina = new OperacoesTiposMaquina(fakeOperacao, fakeTipoMaquina);
        public TipoMaquinaServiceTest()
        {
            _tipoMaquinaRepository = new Mock<ITipoMaquinaRepository>(MockBehavior.Loose);
            _operacaoRepository = new Mock<IOperacaoRepository>(MockBehavior.Loose);
            _operacoesTipoMaquinaRepository = new Mock<IOperacoesTiposMaquinaRepository>(MockBehavior.Loose);

            _tipoMaquinaService = new TipoMaquinaService(_tipoMaquinaRepository.Object,
                                                            _operacaoRepository.Object,
                                                            _operacoesTipoMaquinaRepository.Object);
        }

        [Fact]
        public async void CreateTipoMaquina_OperacoesDontExist_ShouldReturnNull()
        {
            //Arrange

            AddTipoMaquinaCommand fakeAddCommand = new AddTipoMaquinaCommand()
            {
                Nome = "Nome",
                OperacoesId = new List<Guid>() { fakeId1, fakeId2 }
            };

            _operacaoRepository.Setup(w => w.GetOperacaoById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<Operacao>(null));


            //Act

            var tipoMaquina = await _tipoMaquinaService.CreateTipoMaquina(fakeAddCommand);

            //Assert

            Assert.Null(tipoMaquina);
            _operacaoRepository.Verify(x => x.GetOperacaoById(It.IsAny<Guid>()), Times.Exactly(2));
        }

        [Fact]
        public async void CreateTipoMaquina_Valid()
        {
            //Arrange

            AddTipoMaquinaCommand fakeAddCommand = new AddTipoMaquinaCommand()
            {
                Nome = "Nome",
                OperacoesId = new List<Guid>() { fakeId1 }
            };

            _operacaoRepository.Setup(w => w.GetOperacaoById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<Operacao>(fakeOperacao));

            _operacoesTipoMaquinaRepository.Setup(w => w.Add(
                            It.IsAny<OperacoesTiposMaquina>()
                        )).Returns(Task.FromResult(fakeOperacaoTipoMaquina));

            //Act

            var tipoMaquina = await _tipoMaquinaService.CreateTipoMaquina(fakeAddCommand);

            //Assert

            Assert.NotNull(tipoMaquina);
            _operacaoRepository.Verify(x => x.GetOperacaoById(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async void GetOperacoesOfTipoMaquina_TipoMaquinaDoesntExist_ShouldReturnNull()
        {
            //Arrange

            _tipoMaquinaRepository.Setup(w => w.GetTipoMaquinaWithOperacoes(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<TipoMaquina>(null));

            //Act

            var operacoes = await _tipoMaquinaService.GetOperacoesOfTipoMaquina(fakeId1);

            //Assert

            Assert.Null(operacoes);
            _tipoMaquinaRepository.Verify(x => x.GetTipoMaquinaWithOperacoes(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async void GetMaquinasOfTipoMaquina_TipoMaquinaDoesntExist_ShouldReturnNull()
        {
            //Arrange

            _tipoMaquinaRepository.Setup(w => w.GetTipoMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<TipoMaquina>(null));

            //Act

            var maquinas = await _tipoMaquinaService.GetMaquinasOfTipoMaquina(fakeId1);

            //Assert

            Assert.Null(maquinas);
            _tipoMaquinaRepository.Verify(x => x.GetTipoMaquinaById(It.IsAny<Guid>()), Times.Once);
        }

        [Fact]
        public async void ChangeOperacoes_TipoMaquinaDoesntExist_ShouldReturnNull()
        {
            //Arrange

            _tipoMaquinaRepository.Setup(w => w.GetTipoMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<TipoMaquina>(null));

            //Act

            var tipoMaquina = await _tipoMaquinaService.ChangeOperacoes(fakeId1, new UpdateOperacoesTipoMaquinaCommand() { OperacoesId = new List<Guid>() { fakeId2 } });

            //Assert

            Assert.Null(tipoMaquina);
            _tipoMaquinaRepository.Verify(x => x.GetTipoMaquinaById(It.IsAny<Guid>()), Times.Once);
            _operacaoRepository.Verify(x => x.GetOperacoesInList(It.IsAny<List<Guid>>()), Times.Never);
        }

        [Fact]
        public async void ChangeOperacoes_OperacoesDontExist_ShouldReturnNull()
        {
            //Arrange

            _tipoMaquinaRepository.Setup(w => w.GetTipoMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<TipoMaquina>(fakeTipoMaquina));

            _operacaoRepository.Setup(w => w.GetOperacoesInList(
                            It.IsAny<List<Guid>>()
                        )).Returns(Task.FromResult<List<Operacao>>(null));

            //Act

            var tipoMaquina = await _tipoMaquinaService.ChangeOperacoes(fakeTipoMaquina.Id, new UpdateOperacoesTipoMaquinaCommand() { OperacoesId = new List<Guid>() { fakeId1, fakeId2 } });

            //Assert

            Assert.Null(tipoMaquina);
            _tipoMaquinaRepository.Verify(x => x.GetTipoMaquinaById(It.IsAny<Guid>()), Times.Once);
            _operacaoRepository.Verify(x => x.GetOperacoesInList(It.IsAny<List<Guid>>()), Times.Once);
        }

        [Fact]
        public async void ChangeOperacoes_Valid()
        {
            //Arrange

            _tipoMaquinaRepository.Setup(w => w.GetTipoMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<TipoMaquina>(fakeTipoMaquina));

            _operacaoRepository.Setup(w => w.GetOperacoesInList(
                            It.IsAny<List<Guid>>()
                        )).Returns(Task.FromResult<List<Operacao>>(new List<Operacao>() { fakeOperacao, fakeOperacao }));

            //Act

            var tipoMaquina = await _tipoMaquinaService.ChangeOperacoes(fakeTipoMaquina.Id, new UpdateOperacoesTipoMaquinaCommand() { OperacoesId = new List<Guid>() { fakeId1, fakeId2 } });

            //Assert

            Assert.NotNull(tipoMaquina);
            _tipoMaquinaRepository.Verify(x => x.GetTipoMaquinaById(It.IsAny<Guid>()), Times.Once);
            _operacaoRepository.Verify(x => x.GetOperacoesInList(It.IsAny<List<Guid>>()), Times.Once);
        }
    }
}
