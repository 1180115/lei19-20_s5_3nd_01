using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Domain.Services;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Tests.Services
{
    public class MaquinaServiceTest
    {
        private readonly Mock<IMaquinaRepository> _maquinaRepository;

        private MaquinaService _maquinaService;

        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();
        private readonly static Maquina fakeMaquina = new Maquina(new Marca("Marca"), new Modelo("Modelo"), Guid.NewGuid());
        private readonly static Maquina fakeMaquina2 = new Maquina(new Marca("Marca"), new Modelo("Modelo"), fakeId2);
        public MaquinaServiceTest()
        {
            _maquinaRepository = new Mock<IMaquinaRepository>(MockBehavior.Loose);

            _maquinaService = new MaquinaService(_maquinaRepository.Object);
        }

        [Fact]
        public async void AddMaquina_CantSave_ShouldReturnNull()
        {
            //Arrange

            AddMaquinaCommand fakeAddCommand = new AddMaquinaCommand()
            {
                Modelo = "Modelo",
                Marca = "Marca",
                TipoMaquinaId = fakeId1
            };

            _maquinaRepository.Setup(w => w.AddMaquina(
                It.IsAny<Maquina>()
            )).Returns(Task.FromResult<Maquina>(null));


            //Act

            var maquina = await _maquinaService.AddMaquina(fakeAddCommand);

            //Assert

            Assert.Null(maquina);
            _maquinaRepository.Verify(x => x.AddMaquina(It.IsAny<Maquina>()), Times.Once);
        }

        [Fact]
        public async void AddMaquina_Valid()
        {
            //Arrange

            AddMaquinaCommand fakeAddCommand = new AddMaquinaCommand()
            {
                Modelo = "Modelo",
                Marca = "Marca",
                TipoMaquinaId = fakeId1
            };

            _maquinaRepository.Setup(w => w.AddMaquina(
                It.IsAny<Maquina>()
            )).Returns(Task.FromResult(fakeMaquina));


            //Act

            var maquina = await _maquinaService.AddMaquina(fakeAddCommand);

            //Assert

            Assert.NotNull(maquina);
            _maquinaRepository.Verify(x => x.AddMaquina(It.IsAny<Maquina>()), Times.Once);
        }

        [Fact]
        public async void ChangeTipoMaquina_CantFindMaquina_ShouldReturnNull()
        {
            //Arrange

            _maquinaRepository.Setup(w => w.GetMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<Maquina>(null));

            //Act

            var maquina = await _maquinaService.ChangeTipoMaquina(fakeId1, Guid.NewGuid());

            //Assert

            Assert.Null(maquina);
            _maquinaRepository.Verify(x => x.GetMaquinaById(It.IsAny<Guid>()), Times.Once);
            _maquinaRepository.Verify(x => x.UpdateMaquina(It.IsAny<Maquina>()), Times.Never);
        }

        [Fact]
        public async void ChangeTipoMaquina_Valid_ShouldHaveUpdatedTipoMaquinaId()
        {
            //Arrange

            _maquinaRepository.Setup(w => w.GetMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult(fakeMaquina));

            _maquinaRepository.Setup(w => w.UpdateMaquina(
                            It.IsAny<Maquina>()
                        )).Returns(Task.FromResult(fakeMaquina2));
            //Act

            var maquina = await _maquinaService.ChangeTipoMaquina(fakeId1, fakeId2);

            //Assert

            Assert.NotNull(maquina);
            Assert.Equal(fakeId2, maquina.TipoMaquinaId);
            _maquinaRepository.Verify(x => x.GetMaquinaById(It.IsAny<Guid>()), Times.Once);
            _maquinaRepository.Verify(x => x.UpdateMaquina(It.IsAny<Maquina>()), Times.Once);
        }
    }
}
