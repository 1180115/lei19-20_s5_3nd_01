using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Domain.Services;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Tests.Services
{
    public class OperacaoServiceTest
    {
        private readonly Mock<IOperacaoRepository> _operacaoRepository;

        private OperacaoService _operacaoService;

        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();
        private readonly static Operacao fakeOperacao = new Operacao("Descricao", new Duracao(3));

        public OperacaoServiceTest()
        {
            _operacaoRepository = new Mock<IOperacaoRepository>(MockBehavior.Loose);

            _operacaoService = new OperacaoService(_operacaoRepository.Object);
        }

        [Fact]
        public async void AddOperacao_OperacaoAlreadyExists_ShouldReturnNull()
        {
            //Arrange

            AddOperacaoCommand fakeAddCommand = new AddOperacaoCommand()
            {
                Descricao = "Descricao",
                Duracao = 3.7
            };

            _operacaoRepository.Setup(w => w.GetOperacaoByDescricao(
                It.IsAny<string>()
            )).Returns(Task.FromResult<Operacao>(fakeOperacao));


            //Act

            var operacao = await _operacaoService.AddOperacao(fakeAddCommand);

            //Assert

            Assert.Null(operacao);
            _operacaoRepository.Verify(x => x.AddOperacao(It.IsAny<Operacao>()), Times.Never);
        }

    }
}
