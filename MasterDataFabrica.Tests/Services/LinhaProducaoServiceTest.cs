using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Domain.Services;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Tests.Services
{
    public class LinhaProducaoServiceTest
    {
        private readonly Mock<ILinhaProducaoRepository> _linhaProducaoRepository;
        private readonly Mock<IMaquinaRepository> _maquinaRepository;

        private LinhaProducaoService _linhaProducaoService;

        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();
        private readonly static Maquina fakeMaquina = new Maquina(new Marca("Marca"), new Modelo("Modelo"), Guid.NewGuid());
        private readonly static LinhaProducao fakeLinhaProducao = new LinhaProducao("Designacao", new List<Maquina>() { fakeMaquina });
        public LinhaProducaoServiceTest()
        {
            _linhaProducaoRepository = new Mock<ILinhaProducaoRepository>(MockBehavior.Loose);
            _maquinaRepository = new Mock<IMaquinaRepository>(MockBehavior.Loose);

            _linhaProducaoService = new LinhaProducaoService(_linhaProducaoRepository.Object, _maquinaRepository.Object);
        }

        [Fact]
        public async void AddLinhaProducao_CantFindMaquina_ShouldReturnNull()
        {
            //Arrange

            AddLinhaProducaoCommand fakeAddCommand = new AddLinhaProducaoCommand()
            {
                Designacao = "Designacao",
                MaquinasNSlots = new List<Slot>() { new Slot { MaquinaId = fakeId1, Posicao = 1 } }
            };

            _maquinaRepository.Setup(w => w.GetMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult<Maquina>(null));


            //Act

            var linhaProducao = await _linhaProducaoService.AddLinhaProducao(fakeAddCommand);

            //Assert

            Assert.Null(linhaProducao);
            _linhaProducaoRepository.Verify(x => x.AddLinhaProducao(It.IsAny<LinhaProducao>()), Times.Never);
        }

        [Fact]
        public async void AddLinhaProducao_CantSave_ShouldReturnNull()
        {
            //Arrange

            AddLinhaProducaoCommand fakeAddCommand = new AddLinhaProducaoCommand()
            {
                Designacao = "Designacao",
                MaquinasNSlots = new List<Slot>() { new Slot { MaquinaId = fakeId1, Posicao = 1 } }
            };

            _maquinaRepository.Setup(w => w.GetMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult(fakeMaquina));

            _linhaProducaoRepository.Setup(w => w.AddLinhaProducao(
                It.IsAny<LinhaProducao>()
            )).Returns(Task.FromResult<LinhaProducao>(null));


            //Act

            var linhaProducao = await _linhaProducaoService.AddLinhaProducao(fakeAddCommand);

            //Assert

            Assert.Null(linhaProducao);
            _maquinaRepository.Verify(x => x.GetMaquinaById(It.IsAny<Guid>()), Times.Once);
            _linhaProducaoRepository.Verify(x => x.AddLinhaProducao(It.IsAny<LinhaProducao>()), Times.Once);
        }

        [Fact]
        public async void AddLinhaProducao_Valid_ShouldAdd()
        {
            //Arrange

            AddLinhaProducaoCommand fakeAddCommand = new AddLinhaProducaoCommand()
            {
                Designacao = "Designacao",
                MaquinasNSlots = new List<Slot>() { new Slot { MaquinaId = fakeId1, Posicao = 1 } }
            };

            _maquinaRepository.Setup(w => w.GetMaquinaById(
                It.IsAny<Guid>()
            )).Returns(Task.FromResult(fakeMaquina));

            _linhaProducaoRepository.Setup(w => w.AddLinhaProducao(
                It.IsAny<LinhaProducao>()
            )).Returns(Task.FromResult(fakeLinhaProducao));

            //Act

            var linhaProducao = await _linhaProducaoService.AddLinhaProducao(fakeAddCommand);

            //Assert

            Assert.NotNull(linhaProducao);
        }
    }
}
