using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Domain.Services;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Tests.Entities
{
    public class MaquinaTest
    {
        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();
        private readonly static Guid TipoMaquinaId = Guid.NewGuid();
        private readonly static Marca Marca = new Marca("Marca");
        private readonly static Modelo Modelo = new Modelo("Modelo");
        public MaquinaTest()
        { }

        [Fact]
        public async void Maquina_ChangeTipoMaquina_ShouldHaveNewTipoMaquina()
        {
            //Arrange

            var maquina = new Maquina(Marca, Modelo, Guid.NewGuid());

            //Act

            maquina.ChangeTipoMaquina(TipoMaquinaId);


            //Assert

            Assert.NotNull(maquina);
            Assert.Equal(TipoMaquinaId, maquina.TipoMaquinaId);
        }

        [Fact]
        public async void Maquina_Posicao_ShouldBe0()
        {
            //Arrange

            //Act

            var maquina = new Maquina(Marca, Modelo, Guid.NewGuid());

            //Assert

            Assert.NotNull(maquina);
            Assert.Equal(0, maquina.Posicao.NrPosicao);
        }

        [Fact]
        public async void Maquina_ChangePosicao_ShouldHaveNewPosicao()
        {
            //Arrange

            var maquina = new Maquina(Marca, Modelo, Guid.NewGuid());

            //Act

            maquina.ChangePosicao(5);


            //Assert

            Assert.NotNull(maquina);
            Assert.Equal(5, maquina.Posicao.NrPosicao);
        }

        [Fact]
        public async void Maquina_ResetLinhaProducao_ShouldBeReseted()
        {
            //Arrange

            var maquina = new Maquina(Marca, Modelo, Guid.NewGuid());

            //Act

            maquina.ChangePosicao(8);
            maquina.ResetLinhaProducao();

            //Assert

            Assert.NotNull(maquina);
            Assert.Equal(0, maquina.Posicao.NrPosicao);
        }
    }
}
