import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('Test maquina in masterdatafabrica', () => {

  beforeEach( () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('user3@email.com');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
    element(by.buttonText('Encomendas')).click();
    browser.sleep(200);
    element(by.buttonText('Ver Encomendas')).click();
  });

  it('Button exists', () => {
    expect(element(by.buttonText('Adicionar Encomenda'))).toBeDefined();
  });
  it('Button click', () => {
    expect(element(by.buttonText('Adicionar Encomenda')).click());
  });
});
