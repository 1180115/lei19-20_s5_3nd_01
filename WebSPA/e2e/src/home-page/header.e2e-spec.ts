import { element, browser, by } from 'protractor';

describe('Test home-page header', () => {

  beforeEach( () => { browser.get('https://webappwebspa.azurewebsites.net/'); });
  it('Validate homepage title', () => {

    const pageTitle = browser.getTitle();
    expect(pageTitle).toEqual('WebSPA');
  });
/*  it('Validate homepage toolbar', async () => {
    await browser.get('/toolbar');
    expect(await element(by.tagName('mat-toolbar'))).toBeDefined();
  });
  */
});
