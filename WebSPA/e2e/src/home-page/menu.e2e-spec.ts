import { browser, element, by } from 'protractor';

describe('Test homepage menu', () => {
  beforeEach( () => { browser.get('https://webappwebspa.azurewebsites.net/'); });
  it('Validate menu toolbar', async () => {
    expect(await element(by.tagName('mat-toolbar'))).toBeDefined();
  });
  it('Validate menu button MDF', async () => {
    expect(element(by.buttonText('MasterDataFabrica')));
  });
  it('Validate menu button MDP', async () => {
    expect(element(by.buttonText('MasterDataProducao')));
  });
  it('Navigate to operacoes', async () => {
    expect(element(by.buttonText('MasterDataProducao')));
  });
});
