import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';


describe('Test produto in masterdataproducao', () => {

  beforeEach( () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('conf@email.com');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
    element(by.buttonText('MasterDataProducao')).click();
    browser.sleep(200);
    element(by.buttonText('Produto')).click();
  });

  it('Button exists', () => {
    expect(element(by.buttonText('Adicionar Produto'))).toBeDefined();
  });
  it('Button click', () => {
    expect(element(by.buttonText('Adicionar Produto')).click());
  });
  it('Button redirect to create', () => {
    element(by.buttonText('Adicionar Produto')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/produtos/add');
  });
  it('Input nome', () => {
    element(by.buttonText('Adicionar Produto')).click();
    const descricao = element(by.name('inputNome'));
    descricao.sendKeys('nome_001');
    // expect(descricao.getAttribute('ng-reflect-model')).toBe('nome_001');
  });
  it('Button redirect to list', () => {
    element(by.buttonText('Adicionar Produto')).click();
    element(by.buttonText('Voltar')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/produtos');
  });
});
