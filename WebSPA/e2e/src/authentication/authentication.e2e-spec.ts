import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';


describe('Test autenticacao', () => {
  it('Should authenticate user', () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('user3@email.com');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
  });
  it('Should logout', () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('user3@email.com');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
    browser.sleep(2000);
    element(by.buttonText('Logout')).click();
  });

});
