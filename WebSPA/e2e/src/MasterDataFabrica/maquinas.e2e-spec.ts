import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';

describe('Test maquina in masterdatafabrica', () => {

  beforeEach( () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('conf@email.com ');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
    element(by.buttonText('MasterDataFabrica')).click();
    browser.sleep(200);
    element(by.buttonText('Maquinas')).click();
  });

  it('Button exists', () => {
    expect(element(by.buttonText('Adicionar Maquina'))).toBeDefined();
  });
  it('Button click', () => {
    expect(element(by.buttonText('Adicionar Maquina')).click());
  });
  it('Button redirect to create', () => {
    element(by.buttonText('Adicionar Maquina')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/maquinas/add');
  });
  it('Input marca', () => {
    element(by.buttonText('Adicionar Maquina')).click();
    const descricao = element(by.name('inputMarca'));
    descricao.sendKeys('marca_001');
    // expect(descricao.getAttribute('ng-reflect-model')).toBe('marca_001');
  });
  it('Input modelo', () => {
    element(by.buttonText('Adicionar Maquina')).click();
    const descricao = element(by.name('inputModelo'));
    descricao.sendKeys('modelo_001');
    // expect(descricao.getAttribute('ng-reflect-model')).toBe('modelo_001');
  });
  it('Button redirect to list', () => {
    element(by.buttonText('Adicionar Maquina')).click();
    element(by.buttonText('Voltar')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/maquinas');
  });
});
