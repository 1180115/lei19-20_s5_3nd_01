import { browser, element, by } from 'protractor';
import { protractor } from 'protractor/built/ptor';


describe('Test linha-producao in masterdatafabrica', () => {

  beforeEach( () => {
    browser.get('https://webappwebspa.azurewebsites.net/');
    const user = element(by.name('inputUser'));
    user.sendKeys('conf@email.com');
    const password = element(by.name('inputPassword'));
    password.sendKeys('password');
    element(by.buttonText('Login')).click();
    element(by.buttonText('MasterDataFabrica')).click();
    browser.sleep(200);
    element(by.buttonText('LinhasProducao')).click();
  });
  it('Button exists', () => {
    expect(element(by.buttonText('Adicionar Linha de Producao'))).toBeDefined();
  });
  it('Button click', () =>  {
    expect(element(by.buttonText('Adicionar Linha de Producao')).click());
  });
  it('Button redirect to create', () => {
    element(by.buttonText('Adicionar Linha de Producao')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/linhas/add');
  });
  it('Input descricao', () => {
    element(by.buttonText('Adicionar Linha de Producao')).click();
    const descricao = element(by.name('inputDesignacao'));
    descricao.sendKeys('designacao_001');
    // expect(descricao.getAttribute('ng-reflect-model')).toBe('designacao_001');
  });
  it('Button redirect to list', () => {
    element(by.buttonText('Adicionar Linha de Producao')).click();
    element(by.buttonText('Voltar')).click();
    expect(browser.getCurrentUrl()).toBe('https://webappwebspa.azurewebsites.net/linhasproducao');
  });
});
