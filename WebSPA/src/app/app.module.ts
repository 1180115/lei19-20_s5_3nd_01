import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatToolbarModule,
  MatExpansionModule,
  MatMenuModule,
  MatCheckboxModule,
  MatTableModule,
  MatRadioModule,
  MatIconModule
} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from "./home-page/header/header.component";
import { MenuComponent } from './home-page/menu/menu.component';
import { OperacoesComponent } from './MasterDataFabrica/operacoes/operacoes.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MaquinasComponent } from './MasterDataFabrica/maquinas/maquinas.component';
import { LinhasProducaoComponent } from './MasterDataFabrica/linhas-producao/linhas-producao.component';
import { TiposMaquinasComponent } from './MasterDataFabrica/tipos-maquinas/tipos-maquinas.component';
import { ProdutosComponent } from './MasterDataProducao/produtos/produtos.component';
import { HttpClientModule } from '@angular/common/http';
import { AdicionarOperacaoComponent } from './MasterDataFabrica/operacoes/adicionar-operacao/adicionar-operacao.component';
import { AdicionarTiposMaquinasComponent } from './MasterDataFabrica/tipos-maquinas/adicionar-tipos-maquinas/adicionar-tipos-maquinas.component';
import { AdicionarMaquinaComponent } from './MasterDataFabrica/maquinas/adicionar-maquina/adicionar-maquina.component';
import { TipoMaquinasDetailsComponent } from './MasterDataFabrica/tipos-maquinas/tipo-maquinas-details/tipo-maquinas-details.component';
import { ChangeOperacoesComponent } from './MasterDataFabrica/tipos-maquinas/change-operacoes/change-operacoes.component';
import { MaquinaDetailsComponent } from './MasterDataFabrica/maquinas/maquina-details/maquina-details.component';
import { ChangeTipoMaquinaComponent } from './MasterDataFabrica/maquinas/change-tipo-maquina/change-tipo-maquina.component';
import { AdicionarLinhaProducaoComponent } from './MasterDataFabrica/linhas-producao/adicionar-linha-producao/adicionar-linha-producao.component';
import { LinhasProducaoDetailsComponent } from './MasterDataFabrica/linhas-producao/linhas-producao-details/linhas-producao-details.component';
import { ProdutosDetailsComponent } from './MasterDataProducao/produtos/produtos-details/produtos-details.component';
import { AdicionarProdutosComponent } from './MasterDataProducao/produtos/adicionar-produtos/adicionar-produtos.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { HttpModule } from '@angular/http';
import { AuthGuard } from './authGuard';
import { EncomendasComponent } from './GestaoDeEncomendas/encomendas/encomendas.component';
import { AdicionarEncomendasComponent } from './GestaoDeEncomendas/encomendas/adicionar-encomendas/adicionar-encomendas.component';
import { EncomendaDetailsComponent } from './GestaoDeEncomendas/encomendas/encomenda-details/encomenda-details.component';
import { DatePipe } from '@angular/common';
import { ClientesComponent } from './GestaoDeEncomendas/clientes/clientes.component';
import { ClienteDetailsComponent } from './GestaoDeEncomendas/clientes/cliente-details/cliente-details.component';
import { OwnerPageComponent } from './authentication/owner-page/owner-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    OperacoesComponent,
    HomePageComponent,
    MaquinasComponent,
    LinhasProducaoComponent,
    TiposMaquinasComponent,
    ProdutosComponent,
    AdicionarOperacaoComponent,
    AdicionarTiposMaquinasComponent,
    AdicionarMaquinaComponent,
    TipoMaquinasDetailsComponent,
    ChangeOperacoesComponent,
    MaquinaDetailsComponent,
    ChangeTipoMaquinaComponent,
    AdicionarLinhaProducaoComponent,
    LinhasProducaoDetailsComponent,
    ProdutosDetailsComponent,
    AdicionarProdutosComponent,
    LoginComponent,
    RegisterComponent,
    EncomendasComponent,
    AdicionarEncomendasComponent,
    EncomendaDetailsComponent,
    ClientesComponent,
    ClienteDetailsComponent,
    OwnerPageComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatExpansionModule,
    MatMenuModule,
    HttpClientModule,
    MatCheckboxModule,
    MatTableModule,
    MatRadioModule,
    MatIconModule,
    // tslint:disable-next-line: deprecation
    HttpModule
  ],
  providers: [AuthGuard, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
