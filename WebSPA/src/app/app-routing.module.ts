import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperacoesComponent } from './MasterDataFabrica/operacoes/operacoes.component';
import { HomePageComponent } from './home-page/home-page.component';
import { TiposMaquinasComponent } from './MasterDataFabrica/tipos-maquinas/tipos-maquinas.component';
import { MaquinasComponent } from './MasterDataFabrica/maquinas/maquinas.component';
import { LinhasProducaoComponent } from './MasterDataFabrica/linhas-producao/linhas-producao.component';
import { ProdutosComponent } from './MasterDataProducao/produtos/produtos.component';
import { AdicionarOperacaoComponent } from './MasterDataFabrica/operacoes/adicionar-operacao/adicionar-operacao.component';
import { AdicionarTiposMaquinasComponent } from './MasterDataFabrica/tipos-maquinas/adicionar-tipos-maquinas/adicionar-tipos-maquinas.component';
import { AdicionarMaquinaComponent } from './MasterDataFabrica/maquinas/adicionar-maquina/adicionar-maquina.component';
import { AdicionarLinhaProducaoComponent } from './MasterDataFabrica/linhas-producao/adicionar-linha-producao/adicionar-linha-producao.component';
import { AdicionarProdutosComponent } from './MasterDataProducao/produtos/adicionar-produtos/adicionar-produtos.component';
import { LoginComponent } from './authentication/login/login.component';
import { RegisterComponent } from './authentication/register/register.component';
import { AuthGuard } from './authGuard';
import { EncomendasComponent } from './GestaoDeEncomendas/encomendas/encomendas.component';
import { AdicionarEncomendasComponent } from './GestaoDeEncomendas/encomendas/adicionar-encomendas/adicionar-encomendas.component';
import { ClientesComponent } from './GestaoDeEncomendas/clientes/clientes.component';
import { ClienteDetailsComponent } from './GestaoDeEncomendas/clientes/cliente-details/cliente-details.component';
import { OwnerPageComponent } from './authentication/owner-page/owner-page.component';



const routes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'operacoes', component: OperacoesComponent, canActivate: [AuthGuard] },
  { path: 'tiposmaquinas', component: TiposMaquinasComponent, canActivate: [AuthGuard] },
  { path: 'maquinas', component: MaquinasComponent, canActivate: [AuthGuard] },
  { path: 'linhasproducao', component: LinhasProducaoComponent, canActivate: [AuthGuard] },
  { path: 'produtos', component: ProdutosComponent, canActivate: [AuthGuard] },
  { path: 'operacoes/add', component: AdicionarOperacaoComponent, canActivate: [AuthGuard] },
  { path: 'tiposmaquinas/add', component: AdicionarTiposMaquinasComponent, canActivate: [AuthGuard] },
  { path: 'maquinas/add', component: AdicionarMaquinaComponent, canActivate: [AuthGuard] },
  { path: 'linhas/add', component: AdicionarLinhaProducaoComponent, canActivate: [AuthGuard] },
  { path: 'produtos/add', component: AdicionarProdutosComponent, canActivate: [AuthGuard] },
  { path: 'encomendas', component: EncomendasComponent, canActivate: [AuthGuard] },
  { path: 'encomendas/add', component: AdicionarEncomendasComponent, canActivate: [AuthGuard] },
  { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuard] },
  { path: 'perfil', component: ClienteDetailsComponent, canActivate: [AuthGuard] },
  { path: 'owner', component: OwnerPageComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
