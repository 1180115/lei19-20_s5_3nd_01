import { Component, OnInit } from '@angular/core';
import { Cliente } from '../models/cliente';
import { ClientesService } from './clientes.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  clienteId: string;

  showDetails: boolean = false;

  constructor(private clientesService: ClientesService) { }

  ngOnInit() {
    this.getClientes();
  }

  getClientes() {
    if (localStorage.getItem("userType") == "0")
      this.clientesService.getClientes().subscribe(res => this.clientes = res);
  }

  goToDetails(id) {
    this.clienteId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
    this.getClientes();
  }
}
