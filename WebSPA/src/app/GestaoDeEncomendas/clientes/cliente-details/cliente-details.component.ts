import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ClientesService } from '../clientes.service';
import { Cliente } from '../../models/cliente';

@Component({
  selector: 'app-cliente-details',
  templateUrl: './cliente-details.component.html',
  styleUrls: ['./cliente-details.component.css']
})
export class ClienteDetailsComponent implements OnInit {

  @Input() clienteId: string;
  changing: boolean = false;

  realClienteId: string;

  cliente: Cliente;

  userType = localStorage.getItem("userType");
  inputMorada: string;
  inputNome: string;

  @Output() goBackEvent = new EventEmitter();

  constructor(private clientesService: ClientesService) { }

  ngOnInit() {
    this.getCliente();
  }

  getCliente() {
    if (localStorage.getItem("userType") == "0")
      this.clientesService.getClienteSemMorada(this.clienteId).subscribe(res => {
        this.cliente = res;
        this.realClienteId = res._id;
      });
    else
      this.clientesService.getCliente(localStorage.getItem("clienteId")).subscribe(res => {
        this.cliente = res;
        this.realClienteId = res._id;
      });
  }

  cancel() {
    this.changing = false;
  }

  change() {
    this.changing = true;
  }

  confirm() {
    if (this.inputNome && this.inputNome.trim())
      this.cliente.nome = this.inputNome;

    if (this.inputMorada && this.inputMorada.trim())
      this.cliente.morada = this.inputMorada;

    this.clientesService.putCliente(this.cliente).subscribe(res => {
      this.changing = false;
      this.getCliente();
    });
  }

  goBack() {
    this.goBackEvent.emit();
  }
}
