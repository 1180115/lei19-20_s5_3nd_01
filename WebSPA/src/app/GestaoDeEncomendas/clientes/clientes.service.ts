import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cliente } from '../models/cliente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {


  private baseurl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }


  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.baseurl + 'clientes');
  }

  getCliente(id: string): Observable<Cliente> {
    return this.http.get<Cliente>(this.baseurl + 'clientes/' + id);
  }

  getClienteSemMorada(id: string): Observable<Cliente> {
    return this.http.get<Cliente>(this.baseurl + 'clientes/' + id + '/nome');
  }


  putCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(this.baseurl + 'clientes', cliente);
  }
}
