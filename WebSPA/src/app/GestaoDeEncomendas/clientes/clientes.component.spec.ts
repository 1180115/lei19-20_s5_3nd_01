// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { ClientesComponent } from './clientes.component';
import { ClientesService } from './clientes.service';
import { Cliente } from '../models/cliente';

@Injectable()
class MockClientesService {

}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('ClientesComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        ClientesComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: ClientesService, useClass: MockClientesService }
      ]
    }).overrideComponent(ClientesComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(ClientesComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getClientes');
    component.ngOnInit();
    expect(component.getClientes).toHaveBeenCalled();
  });


  it('should run #goToDetails()', async () => {

    component.goToDetails("id");
    expect(component.clienteId).toEqual("id");
    expect(component.showDetails).toBe(true);

  });

  it('should run #backFromDetails()', async () => {
    spyOn(component, 'getClientes');
    component.backFromDetails();
    expect(component.getClientes).toHaveBeenCalled();
    expect(component.showDetails).toBe(false);
  });

});
