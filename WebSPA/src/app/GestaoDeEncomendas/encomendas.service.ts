import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Encomenda } from './models/encomenda';

@Injectable({
  providedIn: 'root'
})
export class EncomendasService {


  private baseurl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }


  getEncomendas(): Observable<Encomenda[]> {
    return this.http.get<Encomenda[]>(this.baseurl + 'encomendas');
  }

  getEncomenda(id: string): Observable<Encomenda> {
    return this.http.get<Encomenda>(this.baseurl + 'encomendas/' + id);
  }

  postEncomenda(addCommand): Observable<Encomenda> {
    return this.http.post<Encomenda>(this.baseurl + 'encomendas', addCommand);
  }

  putEncomenda(encomenda: Encomenda) {
    return this.http.put<Encomenda>(this.baseurl + 'encomendas', encomenda);
  }

  cancelarEncomenda(encomenda: Encomenda) {
    return this.http.put(this.baseurl + 'encomendas/cancelar/' + encomenda._id, encomenda);
  }

  getEncomendasCliente(): Observable<Encomenda[]> {
    return this.http.get<Encomenda[]>(this.baseurl + 'encomendas/cliente/' + localStorage.getItem("clienteId"));
  }
}
