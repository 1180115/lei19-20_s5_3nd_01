// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { EncomendaDetailsComponent } from './encomenda-details.component';
import { EncomendasService } from '../../encomendas.service';
import { Encomenda } from '../../models/encomenda';
import { of } from 'rxjs';

@Injectable()
class MockEncomendasService {

  encomenda = <Encomenda>{
    quantidade: 10
  };

  putEncomenda() {

    return of({});
  }

  getEncomenda() {
    return of(this.encomenda);
  }
}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('EncomendaDetailsComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        EncomendaDetailsComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: EncomendasService, useClass: MockEncomendasService }
      ]
    }).overrideComponent(EncomendaDetailsComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(EncomendaDetailsComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getEncomenda');
    component.ngOnInit();
    expect(component.getEncomenda).toHaveBeenCalled();
  });


  it('should run #goBack()', async () => {
    component.goBackEvent = component.goBackEvent || {};
    spyOn(component.goBackEvent, 'emit');
    component.goBack();
    expect(component.goBackEvent.emit).toHaveBeenCalled();
  });
  
  it('should run #changeEncomenda()', async () => {

    component.changeEncomenda();
    expect(component.changing).toBe(true, 'changing should be true');

  });

  it('should run #cancelChanging()', async () => {

    component.cancelChanging();
    expect(component.changing).toBe(false, 'changing should be false');
  });

  it('should run #confirmEncomenda()', async () => {
    component.inputQuantidade = 10;
    let encomenda = <Encomenda>{
      quantidade: 2
    };
    component.encomenda = encomenda;

    component.confirmEncomenda();

    expect(component.encomenda.quantidade).toEqual(10);
  });

});
