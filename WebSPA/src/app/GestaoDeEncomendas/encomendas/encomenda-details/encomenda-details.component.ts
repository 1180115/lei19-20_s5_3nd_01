import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Encomenda } from '../../models/encomenda';
import { EncomendasService } from '../../encomendas.service';

@Component({
  selector: 'app-encomenda-details',
  templateUrl: './encomenda-details.component.html',
  styleUrls: ['./encomenda-details.component.css']
})
export class EncomendaDetailsComponent implements OnInit {

  changing: boolean = false;
  encomenda: Encomenda;

  inputQuantidade;

  @Input() encomendaId: string;
  @Output() goBackEvent = new EventEmitter();

  showChange = false;

  constructor(
    private encomendasService: EncomendasService
  ) { }

  ngOnInit() {
    this.getEncomenda();
  }

  getEncomenda() {
    this.encomendasService.getEncomenda(this.encomendaId).subscribe(res => this.encomenda = res);
  }

  goBack() {
    this.goBackEvent.emit();
  }

  changeEncomenda() {
    this.changing = true;
  }

  cancelChanging() {
    this.changing = false;
  }

  confirmEncomenda() {

    if (!this.inputQuantidade) {
      return false;
    }

    this.encomenda.quantidade = this.inputQuantidade;

    this.encomendasService.putEncomenda(this.encomenda).subscribe(res => {
      this.changing = false;
      this.getEncomenda();
    });
  }

  getUserType(){
    return localStorage.getItem("userType");
  }
  cancelarEncomenda() {
    this.encomendasService.cancelarEncomenda(this.encomenda).subscribe(res => this.goBack());
  }
}
