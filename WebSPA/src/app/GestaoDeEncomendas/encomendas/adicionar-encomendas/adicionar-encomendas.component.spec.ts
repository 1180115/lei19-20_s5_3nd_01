// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { AdicionarEncomendasComponent } from './adicionar-encomendas.component';
import { ProdutosService } from 'src/app/MasterDataProducao/produtos/produtos.service';
import { Router } from '@angular/router';
import { Produto } from 'src/app/MasterDataProducao/models/produto';

@Injectable()
class MockProdutosService { }

@Injectable()
class MockRouter {
  navigate() { };
}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('AdicionarEncomendasComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        AdicionarEncomendasComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: ProdutosService, useClass: MockProdutosService },
        { provide: Router, useClass: MockRouter }
      ]
    }).overrideComponent(AdicionarEncomendasComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(AdicionarEncomendasComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getProdutos');
    component.ngOnInit();
    // expect(component.getProdutos).toHaveBeenCalled();
  });

  it('should run #updateRadios()', async () => {
    let produto = <Produto>{
      id: "1"
    };

    let produto2 = <Produto>{};
    produto2.toAdd = true;

    component.produtos = [produto, produto2];

    component.updateRadios(produto);
    expect(produto2.toAdd).toBe(false);
    expect(produto.toAdd).toBe(true);
  });

  it('should run #addEncomenda()', async () => {

    component.addEncomenda();

  });

  it('should run #orderTempo()', async () => {
    let produto = <Produto>{
      id: "1",
      tempoFabrico: 2
    };

    let produto2 = <Produto>{
      id: "2",
      tempoFabrico: 1
    };

    component.produtos = [produto, produto2];

    component.orderTempo();
    expect(component.produtos[0].id).toEqual("2");
  });

});
