import { Component, OnInit } from '@angular/core';
import { Produto } from 'src/app/MasterDataProducao/models/produto';
import { ProdutosService } from 'src/app/MasterDataProducao/produtos/produtos.service';
import { Router } from '@angular/router';
import { EncomendasService } from '../../encomendas.service';

@Component({
  selector: 'app-adicionar-encomendas',
  templateUrl: './adicionar-encomendas.component.html',
  styleUrls: ['./adicionar-encomendas.component.css']
})
export class AdicionarEncomendasComponent implements OnInit {

  produtos: Produto[];
  inputQuantidade: number;

  constructor(
    private produtosService: ProdutosService,
    private router: Router,
    private encomendaService: EncomendasService) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos() {
    this.produtosService.getProdutos().subscribe(res => this.produtos = res);
  }

  updateRadios(produto) {
    this.produtos.forEach(x => x.toAdd = false);
    this.produtos.find(x => x.id == produto.id).toAdd = true;
  }

  addEncomenda() {
    if (!this.inputQuantidade)
      return false;

    let produtoId = this.produtos.find(x => x.toAdd == true).id;
    let clienteId = localStorage.getItem("clienteId");

    if (!produtoId || !clienteId)
      return false;

    let addCommand = {
      produtoId: produtoId,
      clienteId: clienteId,
      quantidade: this.inputQuantidade
    }

    this.encomendaService.postEncomenda(addCommand).subscribe(res => {
      if (res) {
        this.router.navigate(['encomendas']);
      }
    }, error => {

    });
  }

  orderTempo() {
    this.produtos.sort((a, b) => a.tempoFabrico - b.tempoFabrico);
  }
}
