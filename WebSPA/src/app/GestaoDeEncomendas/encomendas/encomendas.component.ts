import { Component, OnInit } from '@angular/core';
import { Encomenda } from '../models/encomenda';
import { EncomendasService } from '../encomendas.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-encomendas',
  templateUrl: './encomendas.component.html',
  styleUrls: ['./encomendas.component.css']
})
export class EncomendasComponent implements OnInit {

  encomendas: Encomenda[];

  encomendaId: string;
  showDetails = false;

  constructor(
    private encomendaService: EncomendasService,
    private router: Router) { }

  ngOnInit() {
    this.getEncomendas();
  }

  getEncomendas() {
    if (localStorage.getItem("userType") == "0")
      this.encomendaService.getEncomendas().subscribe(res => this.encomendas = res);
    else
      this.encomendaService.getEncomendasCliente().subscribe(res => this.encomendas = res);
  }

  navigateToAddLinha() {
    this.router.navigate(['encomendas/add']);
  }

  goToDetails(id) {
    this.encomendaId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
    this.getEncomendas();
  }

}
