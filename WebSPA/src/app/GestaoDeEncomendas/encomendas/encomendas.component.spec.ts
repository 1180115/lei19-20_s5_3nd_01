// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError, of } from 'rxjs';

import { Component } from '@angular/core';
import { EncomendasComponent } from './encomendas.component';
import { EncomendasService } from '../encomendas.service';
import { Router } from '@angular/router';
import { Encomenda } from '../models/encomenda';

@Injectable()
class MockEncomendasService {
  encomenda = <Encomenda>{
    _id: "1",
    quantidade: 10
  };

  encomendaCliente = <Encomenda>{
    _id: "2",
    quantidade: 10
  };
  lista: Encomenda[] = [];

  putEncomenda() {

    return of({});
  }

  getEncomendas() {
    this.lista.push(this.encomenda);

    return of(this.lista);
  }

  getEncomendasCliente() {
    this.lista.push(this.encomendaCliente);

    return of(this.lista);
  }
}

@Injectable()
class MockRouter {
  navigate() { };
}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('EncomendasComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        EncomendasComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: EncomendasService, useClass: MockEncomendasService },
        { provide: Router, useClass: MockRouter }
      ]
    }).overrideComponent(EncomendasComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(EncomendasComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getEncomendas');
    component.ngOnInit();
    expect(component.getEncomendas).toHaveBeenCalled();
  });

  it('should run #getEncomendas()', async () => {
    localStorage.setItem("userType", "0");
    component.encomendaService = component.encomendaService || {};
    spyOn(component.encomendaService, 'getEncomendas').and.returnValue(observableOf({}));
    spyOn(component.encomendaService, 'getEncomendasCliente').and.returnValue(observableOf({}));
    component.getEncomendas();
    expect(component.encomendaService.getEncomendas).toHaveBeenCalled();

    localStorage.setItem("userType", "1");
    component.getEncomendas();
    expect(component.encomendaService.getEncomendasCliente).toHaveBeenCalled();
  });

  it('should run #navigateToAddLinha()', async () => {
    component.router = component.router || {};
    spyOn(component.router, 'navigate');
    component.navigateToAddLinha();
    expect(component.router.navigate).toHaveBeenCalled();
  });

  it('should run #goToDetails()', async () => {

    component.goToDetails("1");
    expect(component.encomendaId).toEqual("1");
    expect(component.showDetails).toBe(true);

  });

  it('should run #backFromDetails()', async () => {
    spyOn(component, 'getEncomendas');
    component.backFromDetails();
    expect(component.getEncomendas).toHaveBeenCalled();
    expect(component.showDetails).toBe(false);
  });

});
