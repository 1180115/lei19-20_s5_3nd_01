export class Cliente {
    _id: string;
    nome: string;
    morada: string;

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}
