export class Encomenda {
    _id: string;
    numero: string;
    clienteId: string;
    data: Date;
    total: number;
    taxa_iva: number;
    produtoNome: string;
    quantidade: number;
    preco: number;
    estado: string;
    
    nome: string;

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}
