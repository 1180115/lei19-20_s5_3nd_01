import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Produto } from '../models/produto';
import { HttpClient } from '@angular/common/http';
import { PlanoFabrico } from '../models/plano-fabrico';

@Injectable({
  providedIn: 'root'
})
export class ProdutosService {

  private baseurl = 'https://masterdataproducao.azurewebsites.net/';
  constructor(private http: HttpClient) { }

  getProdutos(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.baseurl + 'api/produto');
  }

  postProduto(command): Observable<Produto> {
    return this.http.post<Produto>(this.baseurl + 'api/produto', command);
  }

  getProduto(id: string): Observable<Produto> {
    return this.http.get<Produto>(this.baseurl + 'api/produto/' + id);
  }

  getPlanoFabricoDeProduto(id: string): Observable<PlanoFabrico[]> {
    return this.http.get<PlanoFabrico[]>(this.baseurl + 'api/produto/' + id + '/planofabrico');
  }

  getProdutosTempoProducao(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.baseurl + 'api/produto/tempoproducao');
  }
}
