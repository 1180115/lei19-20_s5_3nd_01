import { Component, OnInit } from '@angular/core';
import { Produto } from '../models/produto';
import { ProdutosService } from './produtos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.css']
})
export class ProdutosComponent implements OnInit {

  produtos: Produto[];

  produtoId: string;
  showDetails = false;

  constructor(
    private produtosService: ProdutosService,
    private router: Router) { }

  ngOnInit() {
    this.getProdutos();
  }

  getProdutos() {
    this.produtosService.getProdutos().subscribe(res => this.produtos = res);
  }

  navigateToAddProduto() {
    this.router.navigate(['produtos/add']);
  }

  goToDetails(id) {
    this.produtoId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
  }

}
