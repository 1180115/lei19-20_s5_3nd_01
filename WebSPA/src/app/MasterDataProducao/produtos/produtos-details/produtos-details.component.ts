import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Produto } from '../../models/produto';
import { Operacao } from 'src/app/MasterDataFabrica/models/operacao';
import { ProdutosService } from '../produtos.service';

@Component({
  selector: 'app-produtos-details',
  templateUrl: './produtos-details.component.html',
  styleUrls: ['./produtos-details.component.css']
})
export class ProdutosDetailsComponent implements OnInit {



  produto: Produto;
  operacoes: Operacao[];
  @Input() produtoId: string;
  @Output() goBackEvent = new EventEmitter();


  constructor(
    private produtoService: ProdutosService
  ) { }

  ngOnInit() {
    this.getProduto();
  }

  getProduto() {
    this.produtoService.getProduto(this.produtoId).subscribe(res => {
      this.produto = res;
      this.operacoes = res.planoFabrico.operacoes;
    });
  }

  goBack() {
    this.goBackEvent.emit();
  }

}
