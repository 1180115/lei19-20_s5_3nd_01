import { Component, OnInit, Input } from '@angular/core';
import { OperacoesService } from 'src/app/MasterDataFabrica/operacoes/operacoes.service';
import { ProdutosService } from '../produtos.service';
import { Produto } from '../../models/produto';
import { Operacao } from 'src/app/MasterDataFabrica/models/operacao';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adicionar-produtos',
  templateUrl: './adicionar-produtos.component.html',
  styleUrls: ['./adicionar-produtos.component.css']
})
export class AdicionarProdutosComponent implements OnInit {

  operacoes: Operacao[];
  operacoesToAdd: Operacao[] = [];

  errorLabel = false;
  inputNome: string;
  inputPreco: number;

  constructor(
    private operacoesService: OperacoesService,
    private produtoService: ProdutosService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getInfo();
  }

  getInfo() {
    this.operacoesService.getOperacoes().subscribe(res => this.operacoes = res);
  }

  addToList(operacao: Operacao) {
    let index = this.operacoes.findIndex(x => x.id == operacao.id);
    this.operacoes.splice(index, 1);

    this.operacoesToAdd.push(operacao);
  }


  removeFromList(operacao: Operacao) {
    let index = this.operacoesToAdd.findIndex(x => x.id == operacao.id);
    this.operacoesToAdd.splice(index, 1);

    this.operacoes.push(operacao);
  }

  addProduto() {
    this.errorLabel = false;
    if (!this.inputNome || this.operacoesToAdd.length == 0 || !this.inputPreco) {
      return false;
    }
      

    let operacoesId = [];

    for (let i = 0; i <= this.operacoesToAdd.length - 1; i++) {

      let obj = { operacaoId: this.operacoesToAdd[i].id, posicao: i + 1 };
      operacoesId.push(obj);
    }

    let command = {
      nome: this.inputNome,
      operacoesId: operacoesId,
      preco: this.inputPreco
    };

    this.produtoService.postProduto(command).subscribe(
      res => {
        if (res) {
          this.router.navigate(['produtos']);
        }
      }
      , error => {
        this.errorLabel = true;
      }
    );
  }

  goBack() {
    this.router.navigate(['produtos']);
  }
}
