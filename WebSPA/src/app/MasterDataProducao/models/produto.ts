import { PlanoFabrico } from './plano-fabrico';

export class Produto {
    id: string;
    nome: string;
    planoFabrico: PlanoFabrico;
    preco: number;
    tempoFabrico: number;

    toAdd: boolean;

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}