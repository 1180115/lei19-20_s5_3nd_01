import { Operacao } from 'src/app/MasterDataFabrica/models/operacao';

export class PlanoFabrico {
    id: string;
    nome: string;
    operacoes: Operacao[];

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}