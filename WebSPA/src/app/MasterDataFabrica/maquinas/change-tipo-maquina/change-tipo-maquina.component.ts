import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Maquina } from '../../models/maquina';
import { TiposMaquinasService } from '../../tipos-maquinas/tipos-maquinas.service';
import { TipoMaquina } from '../../models/tipo-maquina';
import { MaquinasService } from '../maquinas.service';

@Component({
  selector: 'app-change-tipo-maquina',
  templateUrl: './change-tipo-maquina.component.html',
  styleUrls: ['./change-tipo-maquina.component.css']
})
export class ChangeTipoMaquinaComponent implements OnInit {

  @Input() maquina: Maquina;
  @Output() goBackEvent = new EventEmitter();

  tiposMaquinas: TipoMaquina[];
  constructor(
    private tiposMaquinasService: TiposMaquinasService,
    private maquinasService: MaquinasService
  ) { }

  ngOnInit() {
    this.tiposMaquinasService.getTiposMaquina().subscribe(res => this.tiposMaquinas = res);
  }

  updateRadios(tipo) {
    this.tiposMaquinas.forEach(x => x.toAdd = false);
    this.tiposMaquinas.find(x => x.id == tipo.id).toAdd = true;
  }

  goBack() {
    this.goBackEvent.emit();
  }

  changeTipo() {
    let tipoMaquinaId = this.tiposMaquinas.find(x => x.toAdd == true).id;

    let command = {
      tipoMaquinaId: tipoMaquinaId
    };
    this.maquinasService.changeTipoMaquina(this.maquina.id, command).subscribe(res => this.goBack());
  }
}
