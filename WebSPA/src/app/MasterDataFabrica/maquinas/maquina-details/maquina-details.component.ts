import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { Maquina } from '../../models/maquina';
import { MaquinasService } from '../maquinas.service';
import { TipoMaquina } from '../../models/tipo-maquina';

@Component({
  selector: 'app-maquina-details',
  templateUrl: './maquina-details.component.html',
  styleUrls: ['./maquina-details.component.css']
})
export class MaquinaDetailsComponent implements OnInit {

  maquina: Maquina;

  @Input() maquinaId: string;
  @Output() goBackEvent = new EventEmitter();

  showChange = false;

  constructor(
    private maquinaService: MaquinasService
  ) { }

  ngOnInit() {
    this.getMaquina();
  }

  getMaquina() {
    this.maquinaService.getMaquina(this.maquinaId).subscribe(res => this.maquina = res);
  }

  goBack() {
    this.goBackEvent.emit();
  }

  goToChangeTipo() {
    this.showChange = true;
  }

  backFromChange() {
    this.showChange = false;
    this.getMaquina();
  }

  changeState() {
    this.maquina.active
      ? this.maquinaService.deactivate(this.maquina.id).subscribe(res => this.getMaquina())
      : this.maquinaService.activate(this.maquina.id).subscribe(res => this.getMaquina());
  }
}
