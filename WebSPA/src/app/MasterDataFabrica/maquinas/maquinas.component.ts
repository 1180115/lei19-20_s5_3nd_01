import { Component, OnInit } from '@angular/core';
import { MaquinasService } from './maquinas.service';
import { Router } from '@angular/router';
import { Maquina } from '../models/maquina';

@Component({
  selector: 'app-maquinas',
  templateUrl: './maquinas.component.html',
  styleUrls: ['./maquinas.component.css']
})
export class MaquinasComponent implements OnInit {

  maquinas: Maquina[];

  maquinaId: string;
  showDetails = false;
  constructor(
    private maquinasService: MaquinasService,
    private router: Router) { }

  ngOnInit() {
    this.getMaquinas();
  }

  getMaquinas() {
    this.maquinasService.getMaquinas().subscribe(res => this.maquinas = res);
  }

  navigateToAddMaquina() {
    this.router.navigate(['maquinas/add']);
  }

  goToDetails(id) {
    this.maquinaId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
  }
}
