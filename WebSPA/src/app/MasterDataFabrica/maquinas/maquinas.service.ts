import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Maquina } from '../models/maquina';
import { TipoMaquina } from '../models/tipo-maquina';


@Injectable({
  providedIn: 'root'
})
export class MaquinasService {

  private baseurl = 'https://masterdatafabrica.azurewebsites.net/';
  constructor(private http: HttpClient) { }


  getMaquinas(): Observable<Maquina[]> {
    return this.http.get<Maquina[]>(this.baseurl + 'api/maquina');
  }

  getMaquinasSemLinha(): Observable<Maquina[]> {
    return this.http.get<Maquina[]>(this.baseurl + 'api/maquina/available');

  }

  postMaquina(maquina): Observable<Maquina> {
    return this.http.post<Maquina>(this.baseurl + 'api/maquina', maquina);
  }

  getMaquina(id: string): Observable<Maquina> {
    return this.http.get<Maquina>(this.baseurl + 'api/maquina/' + id);
  }

  changeTipoMaquina(id: string, command) {
    return this.http.put(this.baseurl + 'api/maquina/' + id + '/tipomaquina', command);
  }

  deactivate(id: string) {
    return this.http.patch(this.baseurl + 'api/maquina/' + id + '/deactivate', null);
  }

  activate(id: string) {
    return this.http.patch(this.baseurl + 'api/maquina/' + id + '/activate', null);
  }
}
