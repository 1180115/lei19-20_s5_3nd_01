import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MaquinasService } from '../maquinas.service';
import { TipoMaquina } from '../../models/tipo-maquina';
import { TiposMaquinasService } from '../../tipos-maquinas/tipos-maquinas.service';

@Component({
  selector: 'app-adicionar-maquina',
  templateUrl: './adicionar-maquina.component.html',
  styleUrls: ['./adicionar-maquina.component.css']
})
export class AdicionarMaquinaComponent implements OnInit {

  inputMarca: string;
  inputModelo: string;
  errorLabel = false;

  tiposMaquinas: TipoMaquina[];

  constructor(
    private router: Router,
    private maquinasService: MaquinasService,
    private tiposMaquinasService: TiposMaquinasService
  ) { }

  ngOnInit() {
    this.tiposMaquinasService.getTiposMaquina().subscribe(res => this.tiposMaquinas = res);
  }

  addMaquina() {

    this.errorLabel = false;
    if (!this.inputMarca || !this.inputModelo || !this.tiposMaquinas.find(x => x.toAdd == true)) {
      return false;
    }

    let maquina = {
      marca: this.inputMarca,
      modelo: this.inputModelo,
      tipoMaquinaId: this.tiposMaquinas.find(x => x.toAdd == true).id
    };

    this.maquinasService.postMaquina(maquina).subscribe(
      res => {
        if (res) {
          this.router.navigate(['maquinas']);
        }
      }, error => {
        this.errorLabel = true;
      }
    );
  }

  updateRadios(tipo) {
    this.tiposMaquinas.forEach(x => x.toAdd = false);
    this.tiposMaquinas.find(x => x.id == tipo.id).toAdd = true;
  }

  goBack() {
    this.router.navigate(['maquinas']);
  }

}
