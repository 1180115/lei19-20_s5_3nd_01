import { Duracao } from './duracao';

export class Operacao {
  id: string;
  descricao: string;
  duracao: Duracao;
  toAdd = true;


  constructor(obj?: any) {
    Object.assign(this, obj);
  }

}
