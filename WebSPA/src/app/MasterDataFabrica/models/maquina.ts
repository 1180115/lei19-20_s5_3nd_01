import { TipoMaquina } from './tipo-maquina';

export class Maquina {
    id: string;
    marca: Marca;
    modelo: Modelo;
    tipoMaquina: TipoMaquina;
    active: boolean;

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}

export class Marca {

    nome: string;
    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}

export class Modelo {

    nome: string;
    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}
