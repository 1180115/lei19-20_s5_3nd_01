import { Component, OnInit } from '@angular/core';
import { OperacoesService } from './operacoes.service';
import { Operacao } from '../models/operacao';
import { Router } from '@angular/router';

@Component({
  selector: 'app-operacoes',
  templateUrl: './operacoes.component.html',
  styleUrls: ['./operacoes.component.css']
})
export class OperacoesComponent implements OnInit {


  operacoes: Operacao[];
  constructor(private operacoesService: OperacoesService,
              private router: Router) { }

  ngOnInit() {
    this.operacoesService.getOperacoes().subscribe(res => this.operacoes = res);
  }
  navigateToAddOperacao() {
    this.router.navigate(['operacoes/add']);
  }
}
