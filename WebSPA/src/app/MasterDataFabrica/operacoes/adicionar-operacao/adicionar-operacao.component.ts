import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { OperacoesService } from '../operacoes.service';
import { Operacao } from '../../models/operacao';

@Component({
  selector: 'app-adicionar-operacao',
  templateUrl: './adicionar-operacao.component.html',
  styleUrls: ['./adicionar-operacao.component.css']
})
export class AdicionarOperacaoComponent implements OnInit {

  inputDescricao: string;
  inputDuracao: number;
  errorLabel = false;

  constructor(
    private router: Router,
    private operacoesService: OperacoesService
  ) { }

  ngOnInit() {
  }

  addOperacao() {
    this.errorLabel = false;
    if (!this.inputDescricao || !this.inputDuracao) {
      return false;
    }

    let operacao = {
      descricao: this.inputDescricao,
      duracao: this.inputDuracao
    };

    this.operacoesService.postOperacao(operacao).subscribe(
      res => {
        if (res) {
          this.router.navigate(['operacoes']);
        }
      }, error => {
        this.errorLabel = true;
      }
    );
  }
  goBack() {
    this.router.navigate(['operacoes']);
  }
}
