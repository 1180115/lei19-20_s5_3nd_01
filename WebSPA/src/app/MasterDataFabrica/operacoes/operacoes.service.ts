import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Operacao } from '../models/operacao';
import { Http, ResponseContentType, RequestOptions } from '@angular/http';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OperacoesService {

  private baseurl = 'https://masterdatafabrica.azurewebsites.net/';
  constructor(private http: HttpClient) { }


  getOperacoes(): Observable<Operacao[]> {
    return this.http.get<Operacao[]>(this.baseurl + 'api/operacao');

  }

  postOperacao(operacao): Observable<Operacao> {
    return this.http.post<Operacao>(this.baseurl + 'api/operacao', operacao);
  }

}
