import { Component, OnInit } from '@angular/core';
import { Maquina } from '../../models/maquina';
import { MaquinasService } from '../../maquinas/maquinas.service';
import { LinhasProducaoService } from '../linhas-producao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adicionar-linha-producao',
  templateUrl: './adicionar-linha-producao.component.html',
  styleUrls: ['./adicionar-linha-producao.component.css']
})
export class AdicionarLinhaProducaoComponent implements OnInit {

  maquinas: Maquina[];
  maquinasToAdd: Maquina[] = [];

  errorLabel = false;
  inputDesignacao: string;

  constructor(
    private maquinaService: MaquinasService,
    private linhaProducaoService: LinhasProducaoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getMaquinasSemLinha();
  }

  getMaquinasSemLinha() {
    this.maquinaService.getMaquinasSemLinha().subscribe(res => this.maquinas = res);
  }

  addToList(maquina: Maquina) {
    let index = this.maquinas.findIndex(x => x.id == maquina.id);
    this.maquinas.splice(index, 1);

    this.maquinasToAdd.push(maquina);
  }


  removeFromList(maquina: Maquina) {
    let index = this.maquinasToAdd.findIndex(x => x.id == maquina.id);
    this.maquinasToAdd.splice(index, 1);

    this.maquinas.push(maquina);
  }

  addLinha() {
    this.errorLabel = false;
    if (!this.inputDesignacao || this.maquinasToAdd.length == 0) {
      return false;
    }


    let maquinasNSlots = [];

    for (let i = 0; i <= this.maquinasToAdd.length - 1; i++) {

      let obj = { maquinaId: this.maquinasToAdd[i].id, posicao: i + 1 };
      maquinasNSlots.push(obj);
    }

    let command = {
      designacao: this.inputDesignacao,
      maquinasNSlots: maquinasNSlots
    };

    this.linhaProducaoService.postLinha(command).subscribe(
      res => {
        if (res) {
          this.router.navigate(['linhasproducao']);
        }
      }
      , error => {
        this.errorLabel = true;
      }
    );
  }

  goBack() {
    this.router.navigate(['linhasproducao']);
  }
}
