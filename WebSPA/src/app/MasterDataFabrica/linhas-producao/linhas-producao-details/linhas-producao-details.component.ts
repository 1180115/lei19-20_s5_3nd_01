import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { LinhaProducao } from '../../models/linha-producao';
import { LinhasProducaoService } from '../linhas-producao.service';
import { Maquina } from '../../models/maquina';

@Component({
  selector: 'app-linhas-producao-details',
  templateUrl: './linhas-producao-details.component.html',
  styleUrls: ['./linhas-producao-details.component.css']
})
export class LinhasProducaoDetailsComponent implements OnInit {

  linha: LinhaProducao;
  maquinas: Maquina[];

  @Input() linhaId: string;
  @Output() goBackEvent = new EventEmitter();

  showChange = false;

  constructor(
    private linhasService: LinhasProducaoService
  ) { }

  ngOnInit() {
    this.getLinhaProducao();
  }

  getLinhaProducao() {
    this.linhasService.getLinha(this.linhaId).subscribe(res => this.linha = res);
    this.linhasService.getMaquinasDeLinha(this.linhaId).subscribe(res => this.maquinas = res);
  }

  goBack() {
    this.goBackEvent.emit();
  }
}
