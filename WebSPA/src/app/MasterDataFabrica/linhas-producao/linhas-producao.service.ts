import { Injectable } from '@angular/core';
import { LinhaProducao } from '../models/linha-producao';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Maquina } from '../models/maquina';


@Injectable({
  providedIn: 'root'
})
export class LinhasProducaoService {

  private baseurl = this.newMethod();
  constructor(private http: HttpClient) { }


  private newMethod() {
    return 'https://masterdatafabrica.azurewebsites.net/';
  }

  getLinhas(): Observable<LinhaProducao[]> {
    return this.http.get<LinhaProducao[]>(this.baseurl + 'api/linhaproducao');

  }

  postLinha(command): Observable<LinhaProducao> {
    return this.http.post<LinhaProducao>(this.baseurl + 'api/linhaproducao', command);
  }

  getLinha(id: string): Observable<LinhaProducao> {
    return this.http.get<LinhaProducao>(this.baseurl + 'api/linhaproducao/' + id);
  }

  getMaquinasDeLinha(id: string): Observable<Maquina[]> {
    return this.http.get<Maquina[]>(this.baseurl + 'api/linhaproducao/' + id + '/maquinas');
  }
}
