import { Component, OnInit } from '@angular/core';
import { LinhaProducao } from '../models/linha-producao';
import { LinhasProducaoService } from './linhas-producao.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-linhas-producao',
  templateUrl: './linhas-producao.component.html',
  styleUrls: ['./linhas-producao.component.css']
})
export class LinhasProducaoComponent implements OnInit {

  linhas: LinhaProducao[];

  linhaId: string;
  showDetails = false;
  constructor(
    private linhasProducaoService: LinhasProducaoService,
    private router: Router) { }

  ngOnInit() {
    this.getLinhas();
  }

  getLinhas() {
    this.linhasProducaoService.getLinhas().subscribe(res => this.linhas = res);
  }

  navigateToAddLinha() {
    this.router.navigate(['linhas/add']);
  }

  goToDetails(id) {
    this.linhaId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
  }

}
