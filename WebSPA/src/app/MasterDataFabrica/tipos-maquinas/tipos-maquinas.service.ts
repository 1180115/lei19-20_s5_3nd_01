import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoMaquina } from '../models/tipo-maquina';
import { Operacao } from '../models/operacao';
import { Maquina } from '../models/maquina';


@Injectable({
  providedIn: 'root'
})
export class TiposMaquinasService {


  private headers: Headers;
  private baseurl = 'https://masterdatafabrica.azurewebsites.net/';
  constructor(private http: HttpClient) { }


  getTiposMaquina(): Observable<TipoMaquina[]> {
    return this.http.get<TipoMaquina[]>(this.baseurl + 'api/tipomaquina');

  }

  postTipoMaquina(tipoMaquinaCommand): Observable<TipoMaquina> {
    return this.http.post<TipoMaquina>(this.baseurl + 'api/tipomaquina', tipoMaquinaCommand);
  }

  getTipoMaquina(id: string): Observable<TipoMaquina> {
    return this.http.get<TipoMaquina>(this.baseurl + 'api/tipomaquina/' + id);
  }

  getMaquinasDeTipo(id: string): Observable<Maquina[]> {
    return this.http.get<Maquina[]>(this.baseurl + 'api/tipomaquina/' + id + '/maquinas');
  }

  getOperacoesDeTipo(id: string): Observable<Operacao[]> {
    return this.http.get<Operacao[]>(this.baseurl + 'api/tipomaquina/' + id + '/operacoes');
  }

  changeOperacoesDeTipo(id: string, changeCommand) {
    return this.http.put(this.baseurl + 'api/tipomaquina/' + id + '/operacoes', changeCommand);
  }
}
