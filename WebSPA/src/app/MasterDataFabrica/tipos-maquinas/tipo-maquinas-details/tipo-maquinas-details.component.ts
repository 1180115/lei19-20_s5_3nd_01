import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TiposMaquinasService } from '../tipos-maquinas.service';
import { TipoMaquina } from '../../models/tipo-maquina';
import { Maquina } from '../../models/maquina';
import { Operacao } from '../../models/operacao';

@Component({
  selector: 'app-tipo-maquinas-details',
  templateUrl: './tipo-maquinas-details.component.html',
  styleUrls: ['./tipo-maquinas-details.component.css']
})
export class TipoMaquinasDetailsComponent implements OnInit {

  tipoMaquina: TipoMaquina;
  maquinas: Maquina[];
  operacoes: Operacao[];

  showChangeOperacoes = false;

  @Input() tipoMaquinaId: string;
  @Output() goBackEvent = new EventEmitter();

  constructor(
    private tiposMaquinasService: TiposMaquinasService
  ) { }

  ngOnInit() {
    this.getTotalInfo();
  }

  getTotalInfo() {
    this.tiposMaquinasService.getTipoMaquina(this.tipoMaquinaId).subscribe(res => this.tipoMaquina = res);
    this.tiposMaquinasService.getMaquinasDeTipo(this.tipoMaquinaId).subscribe(res => this.maquinas = res);
    this.tiposMaquinasService.getOperacoesDeTipo(this.tipoMaquinaId).subscribe(res => this.operacoes = res);
  }

  goBack() {
    this.goBackEvent.emit();
  }

  goToChangeOperacoes() {
    this.showChangeOperacoes = true;
  }

  backFromChange() {
    this.showChangeOperacoes = false;
    this.getTotalInfo();
  }
}
