// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { TiposMaquinasComponent } from './tipos-maquinas.component';
import { Router } from '@angular/router';
import { TiposMaquinasService } from './tipos-maquinas.service';

@Injectable()
class MockRouter {
  navigate() {};
}

@Injectable()
class MockTiposMaquinasService {}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({name: 'translate'})
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'phoneNumber'})
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({name: 'safeHtml'})
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('TiposMaquinasComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, ReactiveFormsModule ],
      declarations: [
        TiposMaquinasComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
      providers: [
        { provide: Router, useClass: MockRouter },
        { provide: TiposMaquinasService, useClass: MockTiposMaquinasService }
      ]
    }).overrideComponent(TiposMaquinasComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(TiposMaquinasComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function() {};
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getTiposMaquinas');
    component.ngOnInit();
    // expect(component.getTiposMaquinas).toHaveBeenCalled();
  });

  it('should run #navigateToAddTipoMaquina()', async () => {
    component.router = component.router || {};
    component.navigateToAddTipoMaquina();
    // expect(component.router.navigate).toHaveBeenCalled();
  });

  it('should run #goToDetails()', async () => {

    component.goToDetails({});

  });

});
