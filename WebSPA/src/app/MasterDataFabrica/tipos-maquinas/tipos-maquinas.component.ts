import { Component, OnInit } from '@angular/core';
import { TipoMaquina } from '../models/tipo-maquina';
import { Router } from '@angular/router';
import { TiposMaquinasService } from './tipos-maquinas.service';


@Component({
  selector: 'app-tipos-maquinas',
  templateUrl: './tipos-maquinas.component.html',
  styleUrls: ['./tipos-maquinas.component.css']
})
export class TiposMaquinasComponent implements OnInit {

  tiposMaquinas: TipoMaquina[];

  tipoMaquinaId: string;
  showDetails: boolean = false;

  constructor(
    private router: Router,
    private tiposMaquinasService: TiposMaquinasService
  ) { }

  ngOnInit() {
    this.getTiposMaquinas();
  }

  getTiposMaquinas(){
    this.tiposMaquinasService.getTiposMaquina().subscribe(res => this.tiposMaquinas = res);
  }
  navigateToAddTipoMaquina() {
    this.router.navigate(['tiposmaquinas/add']);
  }

  goToDetails(id) {
    this.tipoMaquinaId = id;
    this.showDetails = true;
  }

  backFromDetails() {
    this.showDetails = false;
    this.getTiposMaquinas();
  }
}
