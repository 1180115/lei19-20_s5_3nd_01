// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { AdicionarTiposMaquinasComponent } from './adicionar-tipos-maquinas.component';
import { Router } from '@angular/router';
import { TiposMaquinasService } from '../tipos-maquinas.service';
import { OperacoesService } from '../../operacoes/operacoes.service';
import { Operacao } from '../../models/operacao';

@Injectable()
class MockRouter {
  navigate() { };
}

@Injectable()
class MockTiposMaquinasService { }

@Injectable()
class MockOperacoesService { }

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('AdicionarTiposMaquinasComponent', () => {
  let fixture;
  let component;
  
  

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        AdicionarTiposMaquinasComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: Router, useClass: MockRouter },
        { provide: TiposMaquinasService, useClass: MockTiposMaquinasService },
        { provide: OperacoesService, useClass: MockOperacoesService }
      ]
    }).overrideComponent(AdicionarTiposMaquinasComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(AdicionarTiposMaquinasComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getOperacoes');
    component.ngOnInit();
    expect(component.getOperacoes).toHaveBeenCalled();
  });


  it('should run #goBack()', async () => {
    component.router = component.router || {};
    spyOn(component.router, 'navigate');
    component.goBack();
    expect(component.router.navigate).toHaveBeenCalled();
  });

  it('should run #updateToAdd()', async () => {
    let operacao = <Operacao>{};
    component.updateToAdd(operacao);
    expect(operacao.toAdd).toBe(true, 'Should switch to true');
    component.updateToAdd(operacao);
    expect(operacao.toAdd).toBe(false, 'Should switch to false');
  });


});
