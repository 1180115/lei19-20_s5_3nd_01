import { Component, OnInit } from '@angular/core';
import { TiposMaquinasService } from '../tipos-maquinas.service';
import { Router } from '@angular/router';
import { Operacao } from '../../models/operacao';
import { OperacoesService } from '../../operacoes/operacoes.service';

@Component({
  selector: 'app-adicionar-tipos-maquinas',
  templateUrl: './adicionar-tipos-maquinas.component.html',
  styleUrls: ['./adicionar-tipos-maquinas.component.css']
})
export class AdicionarTiposMaquinasComponent implements OnInit {

  operacoes: Operacao[];
  inputNome: string;

  constructor(
    private router: Router,
    private tiposMaquinasService: TiposMaquinasService,
    public operacoesService: OperacoesService
  ) { }

  ngOnInit() {
    this.getOperacoes();
  }

  getOperacoes() {
    this.operacoesService.getOperacoes().subscribe(res => {
      this.operacoes = res;
    });
  }


  goBack() {
    this.router.navigate(['tiposmaquinas']);
  }

  updateToAdd(operacao: Operacao) {
    operacao.toAdd === true ? operacao.toAdd = false : operacao.toAdd = true;
  }

  addTipo() {
    if (!this.inputNome) {
      return false;
    }

    let operacoesToAdd: string[] = [];

    this.operacoes.forEach(x => {

      if (x.toAdd) {
        operacoesToAdd.push(x.id);
      }
    });

    if (operacoesToAdd.length == 0) {
      return false;
    }

    let tipoMaquinaCommand = {
      nome: this.inputNome,
      operacoesId: operacoesToAdd
    };
    this.tiposMaquinasService.postTipoMaquina(tipoMaquinaCommand).subscribe(
      res => this.goBack()
    );
  }
}
