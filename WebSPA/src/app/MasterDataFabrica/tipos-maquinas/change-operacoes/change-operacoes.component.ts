import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { TipoMaquina } from '../../models/tipo-maquina';
import { TiposMaquinasService } from '../tipos-maquinas.service';
import { OperacoesService } from '../../operacoes/operacoes.service';
import { Operacao } from '../../models/operacao';

@Component({
  selector: 'app-change-operacoes',
  templateUrl: './change-operacoes.component.html',
  styleUrls: ['./change-operacoes.component.css']
})
export class ChangeOperacoesComponent implements OnInit {

  operacoes: Operacao[];

  @Input() tipoMaquina: TipoMaquina;
  @Output() goBackEvent = new EventEmitter();

  constructor(
    private tiposMaquinasService: TiposMaquinasService,
    private operacoesService: OperacoesService) { }

  ngOnInit() {
    this.getOperacoes();
  }

  getOperacoes() {
    this.operacoesService.getOperacoes().subscribe(res => {
      this.operacoes = res;
    });
  }

  goBack() {
    this.goBackEvent.emit();
  }

  updateToAdd(operacao: Operacao) {
    operacao.toAdd === true ? operacao.toAdd = false : operacao.toAdd = true;
  }

  changeOperacoes() {
    let operacoesToChange: string[] = [];

    this.operacoes.forEach(x => {

      if (x.toAdd) {
        operacoesToChange.push(x.id);
      }
    });

    if (operacoesToChange.length == 0) {
      return false;
    }

    let changeCommand = {
      operacoesId: operacoesToChange
    };
    this.tiposMaquinasService.changeOperacoesDeTipo(this.tipoMaquina.id, changeCommand).subscribe(
      res => this.goBack()
    );
  }

}
