// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError } from 'rxjs';

import { Component } from '@angular/core';
import { ChangeOperacoesComponent } from './change-operacoes.component';
import { TiposMaquinasService } from '../tipos-maquinas.service';
import { OperacoesService } from '../../operacoes/operacoes.service';

@Injectable()
class MockTiposMaquinasService { }

@Injectable()
class MockOperacoesService { }

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('ChangeOperacoesComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        ChangeOperacoesComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: TiposMaquinasService, useClass: MockTiposMaquinasService },
        { provide: OperacoesService, useClass: MockOperacoesService }
      ]
    }).overrideComponent(ChangeOperacoesComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(ChangeOperacoesComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getOperacoes');
    component.ngOnInit();
    expect(component.getOperacoes).toHaveBeenCalled();
  });



  it('should run #goBack()', async () => {
    component.goBackEvent = component.goBackEvent || {};
    spyOn(component.goBackEvent, 'emit');
    component.goBack();
    expect(component.goBackEvent.emit).toHaveBeenCalled();
  });

  it('should run #updateToAdd()', async () => {

    component.updateToAdd({
      toAdd: {}
    });

  });

  it('should run #changeOperacoes()', async () => {
    component.operacoes = component.operacoes || {};
    component.operacoes = ['operacoes'];
    component.tiposMaquinasService = component.tiposMaquinasService || {};
    component.tipoMaquina = component.tipoMaquina || {};
    component.tipoMaquina.id = 'id';
    spyOn(component, 'goBack');
    component.changeOperacoes();
    // expect(component.tiposMaquinasService.changeOperacoesDeTipo).toHaveBeenCalled();
    // expect(component.goBack).toHaveBeenCalled();
  });

});
