import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  login: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  isAuthenticated() {
    return localStorage.getItem("isAuthenticated") == "1";
  }

  toRegister() {
    this.login = false;
  }

  toLogin() {
    this.login = true;
  }
}
