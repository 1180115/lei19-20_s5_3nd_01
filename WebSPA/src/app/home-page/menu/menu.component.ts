import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent {


  constructor(
    private router: Router
  ) {
  }


  navigateToLinhasProducao() {
    this.router.navigate(['linhasproducao']);
  }
  navigateToMaquinas() {
    this.router.navigate(['maquinas']);
  }
  navigateToOperacoes() {
    this.router.navigate(['operacoes']);
  }
  navigateToTiposMaquinas() {
    this.router.navigate(['tiposmaquinas']);
  }
  navigateToProdutos() {
    this.router.navigate(['produtos']);
  }
  navigateToAddEncomendas() {
    this.router.navigate(['encomendas/add']);
  }
  navigateToListEncomendas() {
    this.router.navigate(['encomendas']);
  }
  navigateToClientes() {
    this.router.navigate(['clientes']);
  }
  navigateToPerfil() {
    this.router.navigate(['perfil']);
  }

  isAuthenticated() {
    return localStorage.getItem("isAuthenticated") == "1";
  }

  hasMDFPermission(): boolean {
    return localStorage.getItem("/mdf") != null && localStorage.getItem("userType") != "1";
  }

  hasMDPPermission(): boolean {
    return localStorage.getItem("/mdp") != null && localStorage.getItem("userType") != "1";
  }

  hasEncomendasPermission(): boolean {
    return localStorage.getItem("/encomendas") != null;
  }

  hasClientesPermission(): boolean {
    return localStorage.getItem("/clientes") != null;
  }

  getUserType() {
    return localStorage.getItem("userType");
  }

  logout() {
    localStorage.clear();
    localStorage.setItem("isAuthenticated", "0");
    this.router.navigate(['login']);
  }
}
