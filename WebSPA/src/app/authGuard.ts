import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean {
        if (localStorage.getItem("isAuthenticated") == "1") {

            if (state.url == "/owner" && localStorage.getItem("userType") != "9")
                return false;

            if (state.url == "/clientes" && localStorage.getItem("/clientes") == null)
                return false;

            if (state.url == "/perfil" && localStorage.getItem("userType") != "1")
                return false;

            if (state.url == "/encomendas" && localStorage.getItem("userType") == "9")
                return false;

            if (state.url == "/encomendas/add" && localStorage.getItem("userType") != "1")
                return false;

            if (state.url.includes("/produtos") && localStorage.getItem("/mdp") == null)
                return false;

            if (state.url.includes("/linhas") && localStorage.getItem("/mdf") == null)
                return false;

            if (state.url.includes("/maquinas") && localStorage.getItem("/mdf") == null)
                return false;

            if (state.url.includes("/operacoes") && localStorage.getItem("/mdf") == null)
                return false;

            return true;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
