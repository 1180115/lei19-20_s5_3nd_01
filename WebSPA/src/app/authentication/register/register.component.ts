import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  inputUser: string;
  inputPassword: string;
  inputEmail: string;
  inputMorada: string;

  required: boolean = false;
  emailExists: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  Authenticate() {
    this.required = false;
    this.emailExists = false;

    if (!this.inputUser || !this.inputPassword || !this.inputEmail || !this.inputMorada) {
      this.required = true;
      return false;
    }

    const registerCommand = {
      password: this.inputPassword,
      email: this.inputEmail,
      cliente: {
        nome: this.inputUser,
        morada: this.inputMorada
      }
    };

    // LOGIN
    this.authService.register(registerCommand).subscribe(
      res => {
        if (res) {
          localStorage.setItem('isAuthenticated', "1");

          localStorage.setItem('clienteId', res.cliente);
          localStorage.setItem('userType', "1");

          localStorage.setItem('userId', res._id);

          for (const key in res.permissoes) {
            localStorage.setItem(res.permissoes[key].rota, res.permissoes[key].rota);
          }

          this.router.navigate(['/']);
        }
      }, error => {
        this.emailExists = true;
      }

    );

  }

  toLogin() {
    this.router.navigate(['/login']);
  }
}
