
export class User {
    _id = '';
    email = '';
    cliente = '';
    permissoes: permissao[];

    constructor(obj?: any) {
        Object.assign(this, obj);
    }

}

export class permissao {
    _id: string;
    rota: string;

    constructor(rota: string) {
        this.rota = rota;
    }
}