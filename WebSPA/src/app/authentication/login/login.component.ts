import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  inputEmail: string;
  inputPassword: string;
  errorLabel: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
  }

  Authenticate() {

    this.errorLabel = false;

    if (!this.inputEmail || !this.inputPassword) {
      return false;
    }

    const loginCommand = {
      email: this.inputEmail,
      password: this.inputPassword
    };

    //LOGIN
    this.authService.login(loginCommand).subscribe(
      res => {
        if (res) {

          localStorage.setItem('isAuthenticated', "1");

          if (res.cliente) {
            localStorage.setItem('clienteId', res.cliente);
            localStorage.setItem('userType', "1");

          } else {

            if (res.permissoes.find(x => x.rota == '/owner'))
              localStorage.setItem('userType', "9");
            else
              localStorage.setItem('userType', "0");

          }

          localStorage.setItem('userId', res._id);

          for (const key in res.permissoes) {
            localStorage.setItem(res.permissoes[key].rota, res.permissoes[key].rota);
          }

          if (localStorage.getItem("userType") == "9")
            this.router.navigate(['/owner'])
          else
            this.router.navigate(['/']);
        }
      }, error => {
        this.errorLabel = true;
      }

    );

  }

  toRegister() {
    this.router.navigate(['/register']);
  }
}
