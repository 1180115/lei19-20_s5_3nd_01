import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseurl = 'http://localhost:3000/';
  constructor(private http: HttpClient) { }


  login(loginCommand): Observable<User> {
    return this.http.post<User>(this.baseurl + 'utilizadores/login', loginCommand);
  }

  register(registerCommand): Observable<User> {
    return this.http.post<User>(this.baseurl + 'utilizadores/register', registerCommand);
  }

  registerConfigurador(registerCommand) {
    return this.http.post(this.baseurl + 'utilizadores', registerCommand);
  }

  getConfiguradores(): Observable<User[]> {
    return this.http.get<User[]>(this.baseurl + 'utilizadores/configuradores');
  }

  updateUser(user: User) {
    return this.http.put(this.baseurl + 'utilizadores', user);
  }
}
