import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { User, permissao } from '../models/user';

@Component({
  selector: 'app-owner-page',
  templateUrl: './owner-page.component.html',
  styleUrls: ['./owner-page.component.css']
})
export class OwnerPageComponent implements OnInit {

  users: User[];

  singleUser: User = null;

  mdp: boolean = false;
  mdf: boolean = false;
  clientes: boolean = false;
  encomendas: boolean = false;

  inputEmail: string;
  inputPassword: string;

  errorLabel: boolean = false;

  constructor(
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.getConfiguradores();
  }

  getConfiguradores() {
    this.authService.getConfiguradores().subscribe(res => this.users = res);
  }

  goToDetails(user: User) {
    this.mdf = false;
    this.mdp = false;
    this.clientes = false;
    this.encomendas = false;

    if (user.permissoes.find(x => x.rota == "/clientes"))
      this.clientes = true;

    if (user.permissoes.find(x => x.rota == "/mdp"))
      this.mdp = true;

    if (user.permissoes.find(x => x.rota == "/mdf"))
      this.mdf = true;

    if (user.permissoes.find(x => x.rota == "/encomendas"))
      this.encomendas = true;

    this.singleUser = user;
  }

  updateMDF() {
    this.mdf ? this.mdf = false : this.mdf = true;
  }

  updateMDP() {
    this.mdp ? this.mdp = false : this.mdp = true;
  }

  updateClientes() {
    this.clientes ? this.clientes = false : this.clientes = true;
  }

  updateEncomendas() {
    this.encomendas ? this.encomendas = false : this.encomendas = true;
  }

  addConf() {
    this.errorLabel = false;

    if (!this.inputEmail || !this.inputPassword) {
      this.errorLabel = true;
      return false;
    }

    let registerCommand = {
      email: this.inputEmail,
      password: this.inputPassword
    }

    this.authService.registerConfigurador(registerCommand).subscribe(res => this.getConfiguradores());
  }

  updatePermissions() {
    this.singleUser.permissoes = [];

    if (this.mdf) {
      let perm = new permissao("/mdf");
      this.singleUser.permissoes.push(perm);
    }
    if (this.mdp) {
      let perm = new permissao("/mdp");
      this.singleUser.permissoes.push(perm);
    }
    if (this.clientes) {
      let perm = new permissao("/clientes");
      this.singleUser.permissoes.push(perm);
    }
    if (this.encomendas) {
      let perm = new permissao("/encomendas");
      this.singleUser.permissoes.push(perm);
    }

    this.authService.updateUser(this.singleUser).subscribe(res => this.goBack());
  }

  goBack() {
    this.singleUser = null;
  }
}
