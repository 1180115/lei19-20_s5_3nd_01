// tslint:disable
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Pipe, PipeTransform, Injectable, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, Directive, Input, Output } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable, of as observableOf, throwError, of } from 'rxjs';

import { Component } from '@angular/core';
import { OwnerPageComponent } from './owner-page.component';
import { AuthenticationService } from '../authentication.service';
import { User, permissao } from '../models/user';

@Injectable()
class MockAuthenticationService {
  user = <User>{
    _id: "1",
    permissoes: [{
      rota: "/clientes"
    },
    {
      rota: "/mdf"
    }]
  }
  getConfiguradores() {
    let lista: User[] = [];
    lista.push(this.user);

    return of(lista);
  }

  registerConfigurador() {

  }

  updateUser() {

  }
}

@Directive({ selector: '[oneviewPermitted]' })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'phoneNumber' })
class PhoneNumberPipe implements PipeTransform {
  transform(value) { return value; }
}

@Pipe({ name: 'safeHtml' })
class SafeHtmlPipe implements PipeTransform {
  transform(value) { return value; }
}

describe('OwnerPageComponent', () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        OwnerPageComponent,
        TranslatePipe, PhoneNumberPipe, SafeHtmlPipe,
        OneviewPermittedDirective
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: AuthenticationService, useClass: MockAuthenticationService }
      ]
    }).overrideComponent(OwnerPageComponent, {

    }).compileComponents();
    fixture = TestBed.createComponent(OwnerPageComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () { };
    fixture.destroy();
  });

  it('should run #constructor()', async () => {
    expect(component).toBeTruthy();
  });

  it('should run #ngOnInit()', async () => {
    spyOn(component, 'getConfiguradores');
    component.ngOnInit();
    expect(component.getConfiguradores).toHaveBeenCalled();
  });

  it('should run #getConfiguradores()', async () => {
    component.authService = component.authService || {};
    spyOn(component.authService, 'getConfiguradores').and.returnValue(observableOf({}));
    component.getConfiguradores();
    expect(component.authService.getConfiguradores).toHaveBeenCalled();
  });

  it('should run #goToDetails()', async () => {
    let user = <User>{
      _id: "1",
      permissoes: [{
        rota: "/clientes"
      },
      {
        rota: "/mdf"
      }]
    }

    component.goToDetails(user);

    expect(component.mdf).toBe(true);
    expect(component.mdp).toBe(false);
    expect(component.clientes).toBe(true);
    expect(component.encomendas).toBe(false);
    expect(component.singleUser).toEqual(user);
  });

  it('should run #updateMDF()', async () => {
    component.mdf = true;
    component.updateMDF();
    expect(component.mdf).toBe(false);
  });

  it('should run #updateMDP()', async () => {
    component.mdp = false;
    component.updateMDP();
    expect(component.mdp).toBe(true);
  });

  it('should run #updateClientes()', async () => {
    component.clientes = false;
    component.updateClientes();
    expect(component.clientes).toBe(true);
  });

  it('should run #updateEncomendas()', async () => {
    component.encomendas = true;
    component.updateEncomendas();
    expect(component.encomendas).toBe(false);
  });

  it('should run #addConf()', async () => {
    component.authService = component.authService || {};
    spyOn(component.authService, 'registerConfigurador').and.returnValue(observableOf({}));
    spyOn(component, 'getConfiguradores');
    component.addConf();
    expect(component.errorLabel).toBe(true);

    component.inputEmail = "email";
    component.inputPassword = "password";

    component.addConf();
    expect(component.authService.registerConfigurador).toHaveBeenCalled();
    expect(component.getConfiguradores).toHaveBeenCalled();
  });

  it('should run #updatePermissions()', async () => {
    component.singleUser = component.singleUser || {};
    component.singleUser.permissoes = {
      push: function () { }
    };
    component.authService = component.authService || {};
    spyOn(component.authService, 'updateUser').and.returnValue(observableOf({}));
    spyOn(component, 'goBack');
    component.updatePermissions();
    expect(component.authService.updateUser).toHaveBeenCalled();
    expect(component.goBack).toHaveBeenCalled();
  });

  it('should run #goBack()', async () => {

    component.goBack();
    expect(component.singleUser).toBeNull();
  });

});
