using System;
using Xunit;
using Moq;
using System.Threading.Tasks;
using System.Collections.Generic;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Domain.Services;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Domain.Models;

namespace MasterDataProducao.Tests.Services
{
    public class ProdutoServiceTest
    {
        private readonly Mock<IProdutoRepository> _produtoRepository;
        private readonly Mock<IPlanoFabricoOperacaoRepository> _planoFabricoOperacaoRepository;
        private readonly Mock<IPlanoFabricoRepository> _planoFabricoRepository;

        private ProdutoService _produtoService;

        //VARIAVEIS
        private readonly static Guid fakeId1 = Guid.NewGuid();
        private readonly static Guid fakeId2 = Guid.NewGuid();

        private readonly static Produto fakeProduto = new Produto("Nome", 1);

        public ProdutoServiceTest()
        {
            _produtoRepository = new Mock<IProdutoRepository>(MockBehavior.Loose);

            _produtoService = new ProdutoService(_produtoRepository.Object, _planoFabricoOperacaoRepository.Object, _planoFabricoRepository.Object);
        }

        //[Fact]
        //public async void AddProduto_Valid()
        //{
        //    //Arrange
        //
        //    AddProdutoCommand fakeAddCommand = new AddProdutoCommand()
        //    {
        //        Nome = "Nome",
        //    };
        //
        //    _produtoRepository.Setup(w => w.AddProduto(
        //        It.IsAny<Produto>()
        //    )).Returns(Task.FromResult(fakeProduto));
        //
        //    //Act
        //
        //    var produto = await _produtoService.AddProduto(fakeAddCommand);
        //
        //    //Assert
        //
        //    Assert.NotNull(produto);
        //    _produtoRepository.Verify(x => x.AddProduto(It.IsAny<Produto>()), Times.Once);
        //}
    }
}
