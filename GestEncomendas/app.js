const express = require('express');
const bodyParser = require("body-parser"); 
const app = express();
const port = 3000;

const db_connection = require('./utils/db_connection');
const auth_svc = require('./services/auth_svc');
const utilizadores = require('./api/utilizadores');
const encomendas = require('./api/encomendas');
const clientes = require('./api/clientes');
const stats = require('./api/stats');

app.use(bodyParser.json());
app.use(express.static('public')); 
app.use(bodyParser.urlencoded({ 
  extended: true
})); 

// Autorização do utilizador.
app.use(function validateUser (req, res, next) {
  try {
    const utilizadorId = res.header("X-UtilizadorId");
    const authorized = auth_svc.checkUtilizadorId(utilizadorId);
    if (!authorized) {
      console.log('Utilizador não identificado ligou às: ', Date.now());
      res.status(401).json({message: "Falta header com utilizadorId."});
      res.end();
    } else {
      next();
    }
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app.use('/utilizadores', utilizadores);
app.use('/encomendas', encomendas);
app.use('/clientes', clientes);
app.use('/stats', stats);

app.listen(port, () => console.log('Servidor a escutar na porta ',  port, '!'));