const mongoose = require('mongoose');
const secrets = require('../config/secrets');
const db_uri = 'mongodb+srv://' + secrets.db_user + ':' + secrets.db_pass + '@gestencomendasdb-jkinb.azure.mongodb.net/test?retryWrites=true&w=majority';

mongoose.connect(db_uri, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;

const connection = mongoose.connection;

connection.on('connected', function(){
  console.log("Ligação com sucesso à BD.")
});

connection.on('erro', function(){
  console.log("Erro na ligação à BD.")
});

exports.connection;