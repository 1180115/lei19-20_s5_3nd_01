const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const clienteSchema = new mongoose.Schema({
    nome:   String,
    morada: String,
    estado: String,
    utilizador: { type: ObjectId, ref: 'Utilizador', required: false }  // Relação com Utilizador
});

module.exports = mongoose.model('clientes', clienteSchema);