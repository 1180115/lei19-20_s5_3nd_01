const mongoose = require('mongoose');

const statsSchema = new mongoose.Schema({
    produtoId:   String,
    qtdeEncomendas: Number,
    qtdeEncomendada: Number
});

module.exports = mongoose.model('stats', statsSchema);