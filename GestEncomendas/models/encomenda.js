const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.Schema.Types.ObjectId;

const encomendaSchema = new Schema({
    numero: String,
    cliente: { type: ObjectId, ref: 'Cliente' },
    data: Date,
    taxa_iva: Number,
    produtoId: String,
    produtoNome: String,
    quantidade: Number,
    preco: Number,
    total: Number,
    estado: String
});

module.exports = mongoose.model('Encomendas', encomendaSchema);
