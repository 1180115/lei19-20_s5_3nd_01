const utilizadorModel = require('../models/utilizador');
const clienteModel = require('../models/cliente');

exports.newUtilizador = async function (dadosUtilizador) {
    const utilizadores = await utilizadorModel.find({ email: dadosUtilizador.email }).countDocuments();
    if (utilizadores > 0) {
        return null;
    }
    var utilizador = new utilizadorModel();
    utilizador.email = dadosUtilizador.email;
    utilizador.setPassword(dadosUtilizador.password);

    utilizador.permissoes = [
        {
            "rota": "/encomendas",
            "acessos": "get, post, put"
        },
        {
            "rota": "/clientes",
            "acessos": "get, post, put"
        },
        {
            "rota": "/mdf",
            "acessos": "get, post, put"
        },
        {
            "rota": "/mdp",
            "acessos": "get, post, put"
        }
    ];

    return await utilizador.save();
}

exports.newUtilizadorCliente = async function (dadosUtilizador) {
    const utilizadores = await utilizadorModel.find({ email: dadosUtilizador.email }).countDocuments();
    if (utilizadores > 0) {
        return null;
    }

    const cliente = new clienteModel(dadosUtilizador.cliente);
    var utilizador = new utilizadorModel();
    utilizador.email = dadosUtilizador.email;
    utilizador.cliente = cliente;
    utilizador.setPassword(dadosUtilizador.password);
    // Definir as permissões de Cliente.
    utilizador.permissoes = [
        {
            "rota": "/encomendas",
            "acessos": "get, post, put"
        }
    ];

    cliente.save();
    return await utilizador.save();
}

exports.getUtilizadores = async function () {
    return await utilizadorModel.find({}, { hash: 0, salt: 0 });
}

exports.getConfiguradores = async function () {
    return await utilizadorModel.find({ cliente: null, "permissoes.rota": { $nin: ["/owner"] } }, { hash: 0, salt: 0 });
}

exports.getUtilizador = async function (utilizadorId) {
    return utilizadorModel.findById(utilizadorId, { hash: 0, salt: 0 });
}

exports.deleteUtilizador = async function (utilizadorId) {
    return utilizadorModel.findByIdAndDelete(utilizadorId);
}

exports.updateUtilizador = async function (dadosUtilizador) {
    var myquery = { _id: dadosUtilizador._id };

    return utilizadorModel.update(myquery, dadosUtilizador);
}

exports.validateUtilizador = async function (email, password) {
    const utilizador = await utilizadorModel.findOne({ email: email });
    const validade = utilizador.validatePassword(password);

    if (validade) {
        return utilizador
    } else {
        return null
    }
}