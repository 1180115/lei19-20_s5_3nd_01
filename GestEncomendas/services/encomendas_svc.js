const encomendaModel = require('../models/encomenda');
const clienteModel = require('../models/cliente');

const rp = require('request-promise');

exports.newEncomenda = async function (dadosEncomenda) {
    let encomenda = new encomendaModel(dadosEncomenda);
    let produto;
    let taxaIva = 23;

    var options = {
        uri: "https://masterdataproducao.azurewebsites.net/api/produto/" + dadosEncomenda.produtoId,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true
    };

    await rp(options)
        .then(function (resposta) {
            produto = resposta;
            console.log('produto: ', produto);
        })
        .catch(function (error) {
            throw new Error(error);
        });
    encomenda.cliente = dadosEncomenda.clienteId;
    encomenda.estado = 'Ativa';
    encomenda.preco = produto.preco;
    encomenda.taxa_iva = taxaIva;
    encomenda.total = (produto.preco * dadosEncomenda.quantidade) * (1 + (taxaIva / 100));
    encomenda.produtoNome = produto.nome;
    encomenda.data = Date.now();
    return await encomenda.save();
}

exports.getEncomendas = async function () {
    return await encomendaModel.find({ estado: 'Ativa' });
}

exports.getEncomendasByClienteId = async function (clienteId) {
    return await encomendaModel.find({ cliente: clienteId, estado: 'Ativa' });
}

exports.getEncomenda = async function (encomendaId) {
    return encomendaModel.findById(encomendaId);
}

exports.deleteEncomenda = async function (encomendaId) {
    return encomendaModel.findByIdAndDelete(encomendaId);
}

exports.updateEncomenda = async function (dadosencomenda) {
    dadosencomenda.total = (dadosencomenda.preco * dadosencomenda.quantidade) * (1 + (dadosencomenda.taxa_iva / 100));
    var myquery = { _id: dadosencomenda._id };
    return encomendaModel.update(myquery, dadosencomenda);
}

exports.cancelEncomenda = async function (encomendaId, encomenda) {
    encomenda.estado = 'Cancelada';

    var myquery = { _id: encomendaId };
    return encomendaModel.update(myquery, encomenda);
}
