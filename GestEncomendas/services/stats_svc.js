const statsModel = require('../models/stats');

exports.newStats = async function (statData) {
    const stats = new statsModel(statData);
    return await stats.save();
}

exports.getStats = async function () {
    return await statsModel.find();
}

exports.getStat = async function (statId) {
    return statsModel.findById(StatId);
}

exports.deleteStat = async function (StatId) {
    return statsModel.findByIdAndDelete(StatId);
}

exports.updateStat = async function (statData) {
    var myquery = { _id: statData._id };
    var stats = await statsModel.findOne(myquery);

    if (statData.morada == "" || statData.morada == null)
        statData.morada = stats.morada;

    if (statData.nome == "" || statData.nome == null)
        statData.nome = stats.nome;

    return statsModel.update(myquery, statData);
}
