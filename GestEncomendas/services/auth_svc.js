const utilizadorModel = require('../models/utilizador');

exports.checkUtilizadorId = async function(utilizadorId) {
    const utilizadores = utilizadorModel.findById(utilizadorId).countDocuments();
    if (utilizadores > 0) {
        return true
    } else {
        return false
    }
}