const clienteModel = require('../models/cliente');

exports.newCliente = async function (dadosCliente) {
    const cliente = new clienteModel(dadosCliente);
    return await cliente.save();
}

exports.getClientes = async function () {
    return await clienteModel.find();
}

exports.getCliente = async function (ClienteId) {
    return clienteModel.findById(ClienteId);
}

exports.deleteCliente = async function (ClienteId) {
    return clienteModel.findByIdAndDelete(ClienteId);
}

exports.updateCliente = async function (dadosCliente) {
    var myquery = { _id: dadosCliente._id };
    var cliente = await clienteModel.findOne(myquery);

    if (dadosCliente.morada == "" || dadosCliente.morada == null)
        dadosCliente.morada = cliente.morada;

    if (dadosCliente.nome == "" || dadosCliente.nome == null)
        dadosCliente.nome = cliente.nome;

    return clienteModel.update(myquery, dadosCliente);
}
