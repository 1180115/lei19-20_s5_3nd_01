const express = require('express');
const statsSvc = require('../services/stats_svc');
const router = express.Router()

// Trace function.
router.use(function (req, res, next) {
  next();
})

// Criar nova stat.
router.post('/', async (req, res) => {
  const statDTO = req.body;

  try {
    stat = await statsSvc.newStat(statDTO);
    res.status(201).send(stat)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Listar todas os stats.
router.get('/', async (req, res) => {
  try {
    stats = await statsSvc.getStats();
    res.send(stats)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }
})


// Lista de Stats de um produto.
router.get('/produto/:produtoId', async function (req, res) {
  const produtoid = req.params.produtoId;
  try {
    stats = await statsSvc.getStatsByClienteId(produtoid);
    res.send(stats)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }  
})


// Detalhe de uma stat.
router.get('/:statId', async function (req, res) {
  try {
    stat = await statsSvc.getStat(req.params.statId);
    res.send(stat)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Alterar uma stat.
router.put('/', async function (req, res) {
  const statDTO = req.body;

  try {
    stat = await statsSvc.updateStat(statDTO);
    res.send(stat)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Apagar uma stat.
router.delete('/:statId', async function (req, res) {
  const statid = req.params.statId;

  try {
    stat = await statsSvc.deleteStat(statid);
    res.sendStatus(204)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

module.exports = router










