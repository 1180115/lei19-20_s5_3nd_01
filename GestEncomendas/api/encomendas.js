const express = require('express');
const encomendasSvc = require('../services/encomendas_svc');
const router = express.Router()

// Trace function.
router.use(function (req, res, next) {
  next();
})

// Criar nova encomenda.
router.post('/', async (req, res) => {
  const encomendaDTO = req.body;

  try {
    encomenda = await encomendasSvc.newEncomenda(encomendaDTO);
    res.status(201).send(encomenda)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Listar todas os encomendas.
router.get('/', async (req, res) => {
  try {
    encomendas = await encomendasSvc.getEncomendas();
    res.send(encomendas)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }
})


// Lista de Encomendas de um cliente.
router.get('/cliente/:clienteId', async function (req, res) {
  const clienteid = req.params.clienteId;
  try {
    encomendas = await encomendasSvc.getEncomendasByClienteId(clienteid);
    res.send(encomendas)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }  
})


// Detalhe de uma encomenda.
router.get('/:encomendaId', async function (req, res) {
  try {
    encomenda = await encomendasSvc.getEncomenda(req.params.encomendaId);
    res.send(encomenda)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Alterar uma encomenda.
router.put('/', async function (req, res) {
  const encomendaDTO = req.body;

  try {
    encomenda = await encomendasSvc.updateEncomenda(encomendaDTO);
    res.send(encomenda)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Apagar uma encomenda (para uso administrativo).
router.delete('/:encomendaId', async function (req, res) {
  const encomendaid = req.params.encomendaId;

  try {
    encomenda = await encomendasSvc.deleteEncomenda(encomendaid);
    res.sendStatus(204)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Cancelar uma encomenda (Utilizar em vez de apagar).
router.put('/cancelar/:encomendaId', async function (req, res) {
  const encomendaid = req.params.encomendaId;
  var encomenda = req.body;
  try {
    encomenda = await encomendasSvc.cancelEncomenda(encomendaid, encomenda);
    res.send(encomenda)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

module.exports = router










