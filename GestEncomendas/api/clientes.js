const express = require('express');
const clientesSvc = require('../services/clientes_svc');
const router = express.Router()

// Trace function.
router.use(function (req, res, next) {
  next();
})

// Criar novo cliente.
router.post('/', async (req, res) => {
  const clienteDTO = req.body;

  try {
    cliente = await clientesSvc.newCliente(clienteDTO);
    res.status(201).send(cliente)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Listar todos os clientes.
router.get('/', async (req, res) => {
  try {
    clientes = await clientesSvc.getClientes();
    res.send(clientes)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }
})

// Detalhe de um cliente.
router.get('/:clienteId', async function (req, res) {
  try {
    cliente = await clientesSvc.getCliente(req.params.clienteId);
    res.send(cliente)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

router.get('/:clienteId/nome', async function (req, res) {
  try {
    cliente = await clientesSvc.getCliente(req.params.clienteId);
    cliente.morada = "";
    res.send(cliente)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Alterar um cliente.
router.put('/', async function (req, res) {
  const clienteDTO = req.body;

  try {
    cliente = await clientesSvc.updateCliente(clienteDTO);
    res.send(cliente)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

//Apagar um cliente.
router.delete('/:clienteId', async function (req, res) {
  const clienteid = req.params.clienteId;

  try {
    cliente = await clientesSvc.deleteCliente(clienteid);
    res.sendStatus(204)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

module.exports = router