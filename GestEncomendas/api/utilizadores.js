const express = require('express');
const utilizadoreSvc = require('../services/utilizador_svc');
const router = express.Router();

// Trace function.
router.use(function (req, res, next) {
  next();
})

// Criar novo utilizador.
router.post('/', async (req, res) => {
  const utilizadorDTO = req.body;

  try {
    utilizador = await utilizadoreSvc.newUtilizador(utilizadorDTO);

    if (utilizador){
      res.status(201).send(utilizador)
    } else {
      res.status(400).json({message: 'Mail já existe.'})
    }
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Criar novo utilizador associado a cliente.
router.post('/register', async (req, res) => {
  const utilizadorDTO = req.body;

  try {
    utilizador = await utilizadoreSvc.newUtilizadorCliente(utilizadorDTO);

    if (utilizador){
      res.status(201).send(utilizador)
    } else {
      res.status(400).json({message: 'Mail já existe.'})
    }
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  }
})

// Listar todos os utilizadores.
router.get('/', async (req, res) => {
  try {
    utilizadores = await utilizadoreSvc.getUtilizadores();
    res.send(utilizadores)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }
})

router.get('/configuradores', async (req, res) => {
  try {
    utilizadores = await utilizadoreSvc.getConfiguradores();
    res.send(utilizadores)
  }
  catch (err) {
    res.status(500).json({ message: err.message })
  }
})

// Login.
router.post('/login', async function (req, res) {
  const email = req.body.email;
  const password = req.body.password;
  
  try {
    utilizador = await utilizadoreSvc.validateUtilizador(email, password);

    if(utilizador) {
      res.send(utilizador)
    } else {
      res.send(401)
    }
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Detalhe de um utilizador.
router.get('/:utilizadorId', async function (req, res) {
  try {
    utilizador = await utilizadoreSvc.getUtilizador(req.params.utilizadorId);
    res.send(utilizador)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Alterar um utilizador.
router.put('/', async function (req, res) {
  const utilizadorDTO = req.body;

  try {
    utilizador = await utilizadoreSvc.updateUtilizador(utilizadorDTO);
    res.send(utilizador)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

// Apagar um utilizador.
router.delete('/:utilizadorId', async function (req, res) {
  const utilizadorid = req.params.utilizadorId;

  try {
    utilizador = await utilizadoreSvc.deleteUtilizador(utilizadorid);
    res.sendStatus(204)
  }
  catch (err) {
    res.status(400).json({ message: err.message })
  } 
})

module.exports = router



