namespace MasterDataFabrica.Shared.Command
{
    public interface ICommand
    {
        bool Validate();
    }
}