using System;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Shared.DTOs
{


    public class TipoMaquinaDTO
    {
        public Guid Id;
        public string Nome;
    }
}
