using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;

namespace MasterDataFabrica.Shared.DTOs
{


    public class LinhaProducaoDTO
    {
        public Guid Id;
        public string Designacao;
    }
}
