using System;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Shared.DTOs
{


    public class MaquinaDTO
    {
        public Guid Id;
        public Modelo Modelo;
        public Marca Marca;
        public TipoMaquinaDTO tipoMaquina;
        public bool Active;
    }
}
