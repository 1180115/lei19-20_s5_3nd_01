using System;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Shared.DTOs
{


    public class OperacaoDTO
    {
        public Guid Id;
        public string Descricao;
        public Duracao Duracao;
    }
}
