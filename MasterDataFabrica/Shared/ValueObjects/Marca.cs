namespace MasterDataFabrica.Shared.ValueObjects
{

    public class Marca
    {

        public string Nome { get; private set; }

        public Marca(string nome)
        {
            Nome = nome;
        }
        protected Marca() { }
    }

}