namespace MasterDataFabrica.Shared.ValueObjects
{

    public class Modelo
    {

        public string Nome { get; private set; }

        public Modelo(string nome)
        {
            Nome = nome;
        }
        protected Modelo() { }
    }

}