namespace MasterDataFabrica.Shared.ValueObjects
{

    public class Posicao
    {

        public int NrPosicao { get; private set; }

        public Posicao(int posicao)
        {
            NrPosicao = posicao;
        }
        protected Posicao() { }

        public void ChangePosicao(int posicao)
        {
            NrPosicao = posicao;
        }
    }


}