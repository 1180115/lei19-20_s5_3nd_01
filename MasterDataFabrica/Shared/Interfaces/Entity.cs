using System;

namespace MasterDataFabrica.Shared.Interfaces
{
    //Autor: Diogo 20191003
    public abstract class Entity
    {
        public Guid Id { get; private set; }

        public Entity()
        {
            Id = Guid.NewGuid();
        }

        protected Entity(Guid id)
        {
            Id = id;
        }
    }
}