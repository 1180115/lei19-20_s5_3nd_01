using Microsoft.EntityFrameworkCore;
using MasterDataFabrica.Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MasterDataFabrica.Shared.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<LinhaProducao> LinhasProducao { get; set; }
        public DbSet<Maquina> Maquinas { get; set; }
        public DbSet<TipoMaquina> TiposMaquina { get; set; }
        public DbSet<Operacao> Operacoes { get; set; }
        public DbSet<OperacoesTiposMaquina> OperacoesTiposMaquina { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.ApplyConfiguration(new OperacaoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new MaquinaEntityTypeConfiguration());

            modelBuilder.Entity<OperacoesTiposMaquina>()
                .HasKey(bc => new { bc.OperacaoId, bc.TipoMaquinaId });
            modelBuilder.Entity<OperacoesTiposMaquina>()
                .HasOne(bc => bc.Operacao)
                .WithMany(b => b.OperacoesTiposMaquina)
                .HasForeignKey(bc => bc.OperacaoId);
            modelBuilder.Entity<OperacoesTiposMaquina>()
                .HasOne(bc => bc.TipoMaquina)
                .WithMany(c => c.OperacoesTiposMaquina)
                .HasForeignKey(bc => bc.TipoMaquinaId);
        }

        private class OperacaoEntityTypeConfiguration : IEntityTypeConfiguration<Operacao>
        {
            public void Configure(EntityTypeBuilder<Operacao> operacaoConfiguration)
            {
                operacaoConfiguration.OwnsOne(w => w.Duracao).Property(p => p.Tempo).HasColumnName("Duracao");
            }
        }

        private class MaquinaEntityTypeConfiguration : IEntityTypeConfiguration<Maquina>
        {
            public void Configure(EntityTypeBuilder<Maquina> maquinaConfiguration)
            {
                maquinaConfiguration.OwnsOne(w => w.Posicao).Property(p => p.NrPosicao).HasColumnName("Posicao");
                maquinaConfiguration.OwnsOne(w => w.Marca).Property(p => p.Nome).HasColumnName("Marca");
                maquinaConfiguration.OwnsOne(w => w.Modelo).Property(p => p.Nome).HasColumnName("Modelo");
            }
        }
    }
}