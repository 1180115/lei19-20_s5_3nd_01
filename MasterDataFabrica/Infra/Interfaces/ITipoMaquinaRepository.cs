using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Infra.Interfaces
{


    public interface ITipoMaquinaRepository
    {
        Task<TipoMaquina> AddTipoMaquina(TipoMaquina tipoMaquina);
        Task<List<TipoMaquina>> GetTiposMaquina();
        Task<TipoMaquina> GetTipoMaquinaById(Guid tipoMaquinaId);
        Task<TipoMaquina> UpdateTipoMaquina(TipoMaquina tipoMaquina);
        Task<TipoMaquina> GetTipoMaquinaWithOperacoes(Guid tipoMaquinaId);
        Task<bool> DeleteTipoMaquina(Guid id);
    }
}