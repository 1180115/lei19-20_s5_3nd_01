using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Infra.Interfaces
{


    public interface IOperacoesTiposMaquinaRepository
    {
        Task Add(OperacoesTiposMaquina entity);
        Task RemoveOperacoesDeTipoMaquina(Guid id);
    }
}