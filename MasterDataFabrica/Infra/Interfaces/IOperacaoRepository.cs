using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Infra.Interfaces
{


    public interface IOperacaoRepository
    {

        Task<Operacao> AddOperacao(Operacao operacao);
        Task<Operacao> GetOperacaoById(Guid id);
        Task<Operacao> GetOperacaoByDescricao(string descricao);
        Task<List<Operacao>> GetOperacoes();
        Task<List<Operacao>> GetOperacoesInList(List<Guid> operacoes);
        Task<bool> DeleteOperacao(Guid id);
    }
}