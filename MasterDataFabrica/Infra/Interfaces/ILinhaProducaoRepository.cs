using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Infra.Interfaces
{


    public interface ILinhaProducaoRepository
    {
        Task<LinhaProducao> AddLinhaProducao(LinhaProducao linhaProducao);
        Task<List<LinhaProducao>> GetLinhasProducao();
        Task<LinhaProducao> GetLinhaProducaoById(Guid linhaProducaoId);
        Task<bool> DeleteLinhaProducao(Guid id);
        Task<List<Maquina>> GetMaquinasDeLinha(Guid id);
    }
}