using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Infra.Interfaces
{


    public interface IMaquinaRepository
    {
        Task<Maquina> AddMaquina(Maquina maquina);
        Task<List<Maquina>> GetMaquinas();
        Task<List<Maquina>> GetMaquinasAtivas();
        Task<Maquina> GetMaquinaById(Guid maquinaId);
        Task<Maquina> UpdateMaquina(Maquina maquina);
        Task<bool> DeleteMaquina(Guid id);
        Task<List<Maquina>> GetMaquinasSemLinha();
    }
}