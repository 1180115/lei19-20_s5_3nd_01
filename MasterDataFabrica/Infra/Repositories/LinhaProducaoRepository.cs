using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.Infra.Repositories
{

    public class LinhaProducaoRepository : ILinhaProducaoRepository
    {
        private DataContext _dbContext;
        public LinhaProducaoRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<LinhaProducao> AddLinhaProducao(LinhaProducao linhaProducao)
        {
            if (await _dbContext.LinhasProducao.AnyAsync(x => x.Designacao.Equals(linhaProducao.Designacao)))
                return null;

            var linhaProducaoResult = _dbContext.LinhasProducao.Add(linhaProducao);

            await _dbContext.SaveChangesAsync();

            return linhaProducaoResult.Entity;
        }

        public async Task<List<LinhaProducao>> GetLinhasProducao()
        {
            return await _dbContext.LinhasProducao
                                    .Include(x => x.Maquinas)
                                    .ToListAsync();
        }

        public async Task<LinhaProducao> GetLinhaProducaoById(Guid linhaProducaoId)
        {
            return await _dbContext.LinhasProducao
                                    .Include(x => x.Maquinas)
                                    .SingleOrDefaultAsync(x => x.Id == linhaProducaoId);
        }

        public async Task<bool> DeleteLinhaProducao(Guid id)
        {
            var linhaProducao = await _dbContext.LinhasProducao
                                                .Include(x => x.Maquinas)
                                                .SingleOrDefaultAsync(x => x.Id == id);
            if (linhaProducao == null)
                return false;

            foreach (var m in linhaProducao.Maquinas)
            {
                m.ResetLinhaProducao();
            }

            _dbContext.LinhasProducao.Remove(linhaProducao);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<List<Maquina>> GetMaquinasDeLinha(Guid id)
        {
            return await _dbContext.Maquinas
                                    .Where(x => x.LinhaProducaoId == id)
                                    .OrderBy(x => x.Posicao)
                                    .ToListAsync();
        }

    }
}

