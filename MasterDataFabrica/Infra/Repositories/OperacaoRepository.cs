using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.Infra.Repositories
{

    public class OperacaoRepository : IOperacaoRepository
    {
        private DataContext _dbContext;
        public OperacaoRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Operacao> AddOperacao(Operacao operacao)
        {
            if (await _dbContext.Operacoes.AnyAsync(x => x.Descricao.Equals(operacao.Descricao)))
                return null;

            var result = _dbContext.Operacoes.Add(operacao);

            await _dbContext.SaveChangesAsync();

            return result.Entity;
        }

        public async Task<Operacao> GetOperacaoById(Guid id)
        {
            return await _dbContext.Operacoes.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Operacao> GetOperacaoByDescricao(string descricao)
        {
            return await _dbContext.Operacoes.SingleOrDefaultAsync(x => x.Descricao == descricao);
        }

        public async Task<List<Operacao>> GetOperacoes()
        {
            return await _dbContext.Operacoes.ToListAsync();
        }

        public async Task<List<Operacao>> GetOperacoesInList(List<Guid> operacoes)
        {
            return await _dbContext.Operacoes
                                    .Where(x => operacoes.Contains(x.Id))
                                    .ToListAsync();
        }

        public async Task<bool> DeleteOperacao(Guid id)
        {
            var operacao = await _dbContext.Operacoes.SingleOrDefaultAsync(x => x.Id == id);
            if (operacao == null)
                return false;

            _dbContext.Operacoes.Remove(operacao);
            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}

