using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.Infra.Repositories
{

    public class TipoMaquinaRepository : ITipoMaquinaRepository
    {
        private DataContext _dbContext;
        public TipoMaquinaRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TipoMaquina> AddTipoMaquina(TipoMaquina tipoMaquina)
        {
            if (await _dbContext.TiposMaquina.AnyAsync(x => x.Nome.Equals(tipoMaquina.Nome)))
                return null;

            var tipoMaquinaResult = _dbContext.TiposMaquina.Add(tipoMaquina);

            await _dbContext.SaveChangesAsync();

            return tipoMaquinaResult.Entity;
        }

        public async Task<List<TipoMaquina>> GetTiposMaquina()
        {
            return await _dbContext.TiposMaquina
                                    .Include(x => x.OperacoesTiposMaquina)
                                    .Include(x => x.Maquinas)
                                    .ToListAsync();
        }

        public async Task<TipoMaquina> GetTipoMaquinaById(Guid tipoMaquinaId)
        {
            return await _dbContext.TiposMaquina
                                    .Include(x => x.OperacoesTiposMaquina)
                                    .Include(x => x.Maquinas)
                                    .SingleOrDefaultAsync(x => x.Id == tipoMaquinaId);
        }

        public async Task<TipoMaquina> GetTipoMaquinaWithOperacoes(Guid tipoMaquinaId)
        {
            return await _dbContext.TiposMaquina
                                    .Include(x => x.OperacoesTiposMaquina)
                                        .ThenInclude(x => x.Operacao)
                                    .SingleOrDefaultAsync(x => x.Id == tipoMaquinaId);
        }

        public async Task<TipoMaquina> UpdateTipoMaquina(TipoMaquina tipoMaquina)
        {
            var tipoMaquinaResult = _dbContext.Update(tipoMaquina);
            await _dbContext.SaveChangesAsync();
            return tipoMaquinaResult.Entity;
        }

        public async Task<bool> DeleteTipoMaquina(Guid id)
        {
            var tipoMaquina = await _dbContext.TiposMaquina.SingleOrDefaultAsync(x => x.Id == id);
            if (tipoMaquina == null)
                return false;

            _dbContext.TiposMaquina.Remove(tipoMaquina);
            await _dbContext.SaveChangesAsync();

            return true;
        }
    }
}

