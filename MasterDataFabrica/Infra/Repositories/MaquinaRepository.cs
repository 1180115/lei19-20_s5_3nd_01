using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.Infra.Repositories
{

    public class MaquinaRepository : IMaquinaRepository
    {
        private DataContext _dbContext;
        public MaquinaRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Maquina> AddMaquina(Maquina maquina)
        {
            if (await _dbContext.Maquinas.AnyAsync(x => x.Marca.Nome.Equals(maquina.Marca.Nome) && x.Modelo.Nome.Equals(maquina.Modelo.Nome)))
                return null;

            var maquinaResult = _dbContext.Maquinas.Add(maquina);

            await _dbContext.SaveChangesAsync();

            return maquinaResult.Entity;
        }

        public async Task<List<Maquina>> GetMaquinas()
        {
            return await _dbContext.Maquinas
                                    .Include(x => x.TipoMaquina)
                                    .OrderBy(x => x.Marca.Nome)
                                    .ToListAsync();
        }

        public async Task<List<Maquina>> GetMaquinasAtivas()
        {
            return await _dbContext.Maquinas
                                    .Include(x => x.TipoMaquina)
                                    .Where(x => x.Active)
                                    .OrderBy(x => x.Marca.Nome)
                                    .ToListAsync();
        }

        public async Task<Maquina> GetMaquinaById(Guid maquinaId)
        {
            return await _dbContext.Maquinas
                                    .Include(x => x.TipoMaquina)
                                    .SingleOrDefaultAsync(x => x.Id == maquinaId);
        }

        public async Task<Maquina> UpdateMaquina(Maquina maquina)
        {
            var maquinaResult = _dbContext.Update(maquina);
            await _dbContext.SaveChangesAsync();
            return maquinaResult.Entity;
        }

        public async Task<bool> DeleteMaquina(Guid id)
        {
            var maquina = await _dbContext.Maquinas.SingleOrDefaultAsync(x => x.Id == id);
            if (maquina == null)
                return false;

            _dbContext.Maquinas.Remove(maquina);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<List<Maquina>> GetMaquinasSemLinha()
        {
            return await _dbContext.Maquinas
                                    .Include(x => x.TipoMaquina)
                                    .Where(x => x.LinhaProducaoId == null && x.Active == true)
                                    .ToListAsync();
        }


    }
}

