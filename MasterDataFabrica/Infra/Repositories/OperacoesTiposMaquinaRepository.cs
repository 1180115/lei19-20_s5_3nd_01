using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataFabrica.Infra.Repositories
{

    public class OperacoesTiposMaquinaRepository : IOperacoesTiposMaquinaRepository
    {
        private DataContext _dbContext;
        public OperacoesTiposMaquinaRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(OperacoesTiposMaquina entity)
        {
            _dbContext.OperacoesTiposMaquina.Add(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task RemoveOperacoesDeTipoMaquina(Guid id)
        {
            var listaTipos = await _dbContext.OperacoesTiposMaquina.Where(x => x.TipoMaquinaId == id).ToListAsync();

            _dbContext.RemoveRange(listaTipos);

            await _dbContext.SaveChangesAsync();
        }
    }
}

