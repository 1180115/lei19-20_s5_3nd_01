﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFabrica.Migrations
{
    public partial class OpTm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OperacoesTiposMaquina",
                table: "OperacoesTiposMaquina");

            migrationBuilder.DropIndex(
                name: "IX_OperacoesTiposMaquina_OperacaoId",
                table: "OperacoesTiposMaquina");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperacoesTiposMaquina",
                table: "OperacoesTiposMaquina",
                columns: new[] { "OperacaoId", "TipoMaquinaId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_OperacoesTiposMaquina",
                table: "OperacoesTiposMaquina");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OperacoesTiposMaquina",
                table: "OperacoesTiposMaquina",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_OperacoesTiposMaquina_OperacaoId",
                table: "OperacoesTiposMaquina",
                column: "OperacaoId");
        }
    }
}
