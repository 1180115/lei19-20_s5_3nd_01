﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFabrica.Migrations
{
    public partial class OperacoesTiposMaquinas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Operacoes_TiposMaquina_TipoMaquinaId",
                table: "Operacoes");

            migrationBuilder.DropIndex(
                name: "IX_Operacoes_TipoMaquinaId",
                table: "Operacoes");

            migrationBuilder.DropColumn(
                name: "TipoMaquinaId",
                table: "Operacoes");

            migrationBuilder.CreateTable(
                name: "OperacoesTiposMaquina",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    OperacaoId = table.Column<Guid>(nullable: false),
                    TipoMaquinaId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperacoesTiposMaquina", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperacoesTiposMaquina_Operacoes_OperacaoId",
                        column: x => x.OperacaoId,
                        principalTable: "Operacoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OperacoesTiposMaquina_TiposMaquina_TipoMaquinaId",
                        column: x => x.TipoMaquinaId,
                        principalTable: "TiposMaquina",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OperacoesTiposMaquina_OperacaoId",
                table: "OperacoesTiposMaquina",
                column: "OperacaoId");

            migrationBuilder.CreateIndex(
                name: "IX_OperacoesTiposMaquina_TipoMaquinaId",
                table: "OperacoesTiposMaquina",
                column: "TipoMaquinaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OperacoesTiposMaquina");

            migrationBuilder.AddColumn<Guid>(
                name: "TipoMaquinaId",
                table: "Operacoes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Operacoes_TipoMaquinaId",
                table: "Operacoes",
                column: "TipoMaquinaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Operacoes_TiposMaquina_TipoMaquinaId",
                table: "Operacoes",
                column: "TipoMaquinaId",
                principalTable: "TiposMaquina",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
