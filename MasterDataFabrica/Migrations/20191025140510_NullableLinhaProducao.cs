﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataFabrica.Migrations
{
    public partial class NullableLinhaProducao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas");

            migrationBuilder.AlterColumn<Guid>(
                name: "LinhaProducaoId",
                table: "Maquinas",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas",
                column: "LinhaProducaoId",
                principalTable: "LinhasProducao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas");

            migrationBuilder.AlterColumn<Guid>(
                name: "LinhaProducaoId",
                table: "Maquinas",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Maquinas_LinhasProducao_LinhaProducaoId",
                table: "Maquinas",
                column: "LinhaProducaoId",
                principalTable: "LinhasProducao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
