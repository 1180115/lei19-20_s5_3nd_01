using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.Command;

namespace MasterDataFabrica.Domain.Commands
{


    public class AddTipoMaquinaCommand : ICommand
    {

        public string Nome;
        public List<Guid> OperacoesId;

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Nome) ||
                OperacoesId.Count == 0)
            {
                return false;
            }
            return true;
        }
    }
}

