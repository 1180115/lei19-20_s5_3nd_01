using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.Command;

namespace MasterDataFabrica.Domain.Commands
{
    public class AddLinhaProducaoCommand : ICommand
    {

        public string Designacao;
        public List<Slot> MaquinasNSlots;

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Designacao) ||
                MaquinasNSlots.Count == 0)
            {
                return false;
            }
            return true;
        }
    }

    public class Slot
    {
        public Guid MaquinaId;
        public int Posicao;
    }
}

