using MasterDataFabrica.Shared.Command;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Commands
{


    public class AddOperacaoCommand : ICommand
    {

        public string Descricao;
        public double Duracao;

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Descricao) ||
                Duracao <= 0)
            {
                return false;
            }
            return true;
        }
    }
}

