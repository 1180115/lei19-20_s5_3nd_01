using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.Command;

namespace MasterDataFabrica.Domain.Commands
{


    public class UpdateOperacoesTipoMaquinaCommand : ICommand
    {

        public List<Guid> OperacoesId;

        public bool Validate()
        {
            if (OperacoesId.Count == 0)
            {
                return false;
            }
            return true;
        }
    }
}

