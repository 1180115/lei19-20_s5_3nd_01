using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.Command;

namespace MasterDataFabrica.Domain.Commands
{


    public class AddMaquinaCommand : ICommand
    {

        public string Modelo;
        public string Marca;
        public Guid TipoMaquinaId;

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Modelo) ||
                string.IsNullOrWhiteSpace(Marca) ||
                TipoMaquinaId == Guid.Empty)
            {
                return false;
            }
            return true;
        }
    }
}

