using System;
using System.Collections.Generic;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Shared.Command;

namespace MasterDataFabrica.Domain.Commands
{


    public class ChangeTipoMaquinaDeMaquinaCommand : ICommand
    {

        public Guid TipoMaquinaId;

        public bool Validate()
        {
            if (TipoMaquinaId == Guid.Empty)
            {
                return false;
            }
            return true;
        }
    }
}