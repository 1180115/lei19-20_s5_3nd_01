using System.Collections.Generic;
using System.Linq;
using MasterDataFabrica.Shared.Interfaces;

namespace MasterDataFabrica.Domain.Models
{

    public class TipoMaquina : Entity
    {
        public string Nome { get; private set; }
        public List<OperacoesTiposMaquina> OperacoesTiposMaquina { get; private set; }
        public List<Maquina> Maquinas { get; private set; }

        public TipoMaquina(string nome)
        {
            Nome = nome;
        }
        protected TipoMaquina() { }

    }
}
