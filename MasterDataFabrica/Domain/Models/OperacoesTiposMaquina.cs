using System;
using System.Collections.Generic;
using MasterDataFabrica.Shared.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Models
{

    public class OperacoesTiposMaquina
    {
        public Guid OperacaoId { get; set; }
        public Operacao Operacao { get; set; }

        public Guid TipoMaquinaId { get; set; }
        public TipoMaquina TipoMaquina { get; set; }

        protected OperacoesTiposMaquina() { }

        public OperacoesTiposMaquina(Operacao operacao, TipoMaquina tipoMaquina)
        {
            Operacao = operacao;
            TipoMaquina = tipoMaquina;
        }
    }
}
