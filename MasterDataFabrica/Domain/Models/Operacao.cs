using System.Collections.Generic;
using MasterDataFabrica.Shared.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Models
{

    public class Operacao : Entity
    {


        public string Descricao { get; private set; }
        public Duracao Duracao { get; private set; }

        public List<OperacoesTiposMaquina> OperacoesTiposMaquina { get; private set; }
        
        public Operacao(string descricao, Duracao duracao)
        {
            Descricao = descricao;
            Duracao = duracao;
        }
        protected Operacao() { }
    }

}
