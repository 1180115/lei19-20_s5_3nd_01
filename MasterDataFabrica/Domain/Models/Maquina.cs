using System;
using System.ComponentModel.DataAnnotations.Schema;
using MasterDataFabrica.Shared.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Models
{

    public class Maquina : Entity
    {
        public Modelo Modelo { get; private set; }
        public Marca Marca { get; private set; }
        public Posicao Posicao { get; private set; }
        public TipoMaquina TipoMaquina { get; private set; }
        public Guid TipoMaquinaId { get; private set; }
        public bool Active { get; private set; }

        [ForeignKey("LinhaProducaoId")]
        public LinhaProducao linhaProducao { get; private set; }
        public Guid? LinhaProducaoId { get; private set; }

        public Maquina(Marca marca, Modelo modelo, Guid tipoMaquinaId)
        {
            Marca = marca;
            Modelo = modelo;
            TipoMaquinaId = tipoMaquinaId;
            Posicao = new Posicao(0);
            Active = true;
        }
        protected Maquina() { }

        public void ChangeTipoMaquina(Guid tipoMaquinaId)
        {
            if (tipoMaquinaId != null)
            {
                TipoMaquinaId = tipoMaquinaId;
            }
        }

        public void ChangePosicao(int posicao)
        {
            Posicao.ChangePosicao(posicao);
        }

        public void ResetLinhaProducao()
        {
            Posicao.ChangePosicao(0);
            LinhaProducaoId = null;
        }

        public void ActivateMaquina()
        {
            Active = true;
        }

        public void DeactivateMaquina()
        {
            Active = false;
        }
    }


}
