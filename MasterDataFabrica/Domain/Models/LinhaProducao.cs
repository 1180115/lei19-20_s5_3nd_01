using System.Collections.Generic;
using MasterDataFabrica.Shared.Interfaces;

namespace MasterDataFabrica.Domain.Models
{

    public class LinhaProducao : Entity
    {
        public string Designacao { get; private set; }
        public List<Maquina> Maquinas { get; private set; }

        protected LinhaProducao() { }

        public LinhaProducao(string designacao, List<Maquina> maquinas)
        {
            Designacao = designacao;
            Maquinas = maquinas;
        }
    }
}
