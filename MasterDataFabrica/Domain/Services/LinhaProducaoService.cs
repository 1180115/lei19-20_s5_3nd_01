using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Services
{
    public class LinhaProducaoService : ILinhaProducaoService
    {
        private readonly ILinhaProducaoRepository _linhaProducaoRepository;
        private readonly IMaquinaRepository _maquinaRepository;
        public LinhaProducaoService(ILinhaProducaoRepository linhaProducaoRepository, IMaquinaRepository maquinaRepository)
        {
            _linhaProducaoRepository = linhaProducaoRepository;
            _maquinaRepository = maquinaRepository;
        }

        public async Task<LinhaProducao> AddLinhaProducao(AddLinhaProducaoCommand command)
        {

            List<Maquina> maquinas = new List<Maquina>();

            foreach (var m in command.MaquinasNSlots)
            {
                Maquina maquina = await _maquinaRepository.GetMaquinaById(m.MaquinaId);
                if (maquina != null)
                {
                    maquina.ChangePosicao(m.Posicao);
                    maquinas.Add(maquina);
                }
            }

            if (maquinas == null || maquinas.Count != command.MaquinasNSlots.Count)
                return null;

            LinhaProducao linhaProducao = new LinhaProducao(command.Designacao, maquinas);

            LinhaProducao linhaProducaoResult = await _linhaProducaoRepository.AddLinhaProducao(linhaProducao);

            if (linhaProducaoResult == null)
                return null;

            return linhaProducaoResult;
        }
    }
}