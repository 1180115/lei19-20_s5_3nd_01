using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;

namespace MasterDataFabrica.Domain.Services
{
    public class TipoMaquinaService : ITipoMaquinaService
    {
        private readonly ITipoMaquinaRepository _tipoMaquinaRepository;
        private readonly IOperacaoRepository _operacaoRepository;
        private readonly IOperacoesTiposMaquinaRepository _operacoesTiposMaquinaRepository;

        public TipoMaquinaService(ITipoMaquinaRepository tipoMaquinaRepository,
         IOperacaoRepository operacaoRepository,
        IOperacoesTiposMaquinaRepository operacoesTiposMaquinaRepository)
        {
            _tipoMaquinaRepository = tipoMaquinaRepository;
            _operacaoRepository = operacaoRepository;
            _operacoesTiposMaquinaRepository = operacoesTiposMaquinaRepository;
        }

        public async Task<TipoMaquina> CreateTipoMaquina(AddTipoMaquinaCommand command)
        {
            List<Operacao> operacoes = new List<Operacao>();

            foreach (var operacaoId in command.OperacoesId)
            {
                Operacao op = await _operacaoRepository.GetOperacaoById(operacaoId);
                if (op != null)
                    operacoes.Add(op);
            }

            if (operacoes.Count != command.OperacoesId.Count)
                return null;

            List<OperacoesTiposMaquina> operacoesTipoMaquina = new List<OperacoesTiposMaquina>();

            TipoMaquina tipoMaquina = new TipoMaquina(command.Nome);

            foreach (var operacao in operacoes)
            {
                await _operacoesTiposMaquinaRepository.Add(new OperacoesTiposMaquina(operacao, tipoMaquina));
            }

            return tipoMaquina;
        }

        public async Task<TipoMaquina> ChangeOperacoes(Guid id, UpdateOperacoesTipoMaquinaCommand command)
        {
            TipoMaquina tipoMaquina = await _tipoMaquinaRepository.GetTipoMaquinaById(id);

            if (tipoMaquina == null)
                return null;

            List<Operacao> newOperacoes = await _operacaoRepository.GetOperacoesInList(command.OperacoesId);

            if (newOperacoes == null || newOperacoes.Count == 0)
                return null;

            await _operacoesTiposMaquinaRepository.RemoveOperacoesDeTipoMaquina(id);

            foreach (var operacao in newOperacoes)
            {
                await _operacoesTiposMaquinaRepository.Add(new OperacoesTiposMaquina(operacao, tipoMaquina));
            }

            return tipoMaquina;
        }

        public async Task<List<Operacao>> GetOperacoesOfTipoMaquina(Guid id)
        {
            TipoMaquina tipo = await _tipoMaquinaRepository.GetTipoMaquinaWithOperacoes(id);

            if (tipo == null)
                return null;

            return tipo.OperacoesTiposMaquina.Select(x => x.Operacao).ToList();
        }

        public async Task<List<Maquina>> GetMaquinasOfTipoMaquina(Guid id)
        {
            TipoMaquina tipo = await _tipoMaquinaRepository.GetTipoMaquinaById(id);

            if (tipo == null)
                return null;

            return tipo.Maquinas;
        }

        
    }
}