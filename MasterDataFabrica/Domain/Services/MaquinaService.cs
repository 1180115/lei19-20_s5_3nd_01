using System;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Services
{
    public class MaquinaService : IMaquinaService
    {
        private readonly IMaquinaRepository _maquinaRepository;
        public MaquinaService(IMaquinaRepository maquinaRepository)
        {
            _maquinaRepository = maquinaRepository;
        }

        public async Task<Maquina> AddMaquina(AddMaquinaCommand command)
        {
            Maquina maquina = new Maquina(
                                            new Marca(command.Marca),
                                            new Modelo(command.Modelo),
                                            command.TipoMaquinaId);

            Maquina maquinaResult = await _maquinaRepository.AddMaquina(maquina);

            if (maquinaResult == null)
                return null;

            return maquinaResult;
        }

        public async Task<Maquina> ChangeTipoMaquina(Guid id, Guid tipoMaquinaId)
        {
            Maquina maquina = await _maquinaRepository.GetMaquinaById(id);

            if (maquina == null)
                return null;

            maquina.ChangeTipoMaquina(tipoMaquinaId);

            return await _maquinaRepository.UpdateMaquina(maquina);
        }

        public async Task<bool> ChangeMaquinaState(Guid maquinaId, bool state)
        {
            Maquina maquina = await _maquinaRepository.GetMaquinaById(maquinaId);

            if (maquina == null)
                return false;

            if (state == true)
                maquina.ActivateMaquina();
            else
                maquina.DeactivateMaquina();

            await _maquinaRepository.UpdateMaquina(maquina);

            //DESPOLETAR O PROCESSO REPLANEAMENTO AQUI

            return true;
        }
    }
}