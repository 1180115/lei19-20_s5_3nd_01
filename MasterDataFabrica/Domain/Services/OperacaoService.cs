using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.ValueObjects;

namespace MasterDataFabrica.Domain.Services
{
    public class OperacaoService : IOperacaoService
    {
        private readonly IOperacaoRepository _operacaoRepository;
        public OperacaoService(IOperacaoRepository operacaoRepository)
        {
            _operacaoRepository = operacaoRepository;
        }

        public async Task<Operacao> AddOperacao(AddOperacaoCommand command)
        {
            Operacao checkOperacao = await _operacaoRepository.GetOperacaoByDescricao(command.Descricao);
            
            if (checkOperacao != null)
                return null;

            Operacao operacao = new Operacao(command.Descricao, new Duracao(command.Duracao));

            Operacao operacaoResult = await _operacaoRepository.AddOperacao(operacao);

            return operacaoResult;
        }
    }
}