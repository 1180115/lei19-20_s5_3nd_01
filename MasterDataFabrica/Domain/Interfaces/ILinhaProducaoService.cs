using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;

namespace MasterDataFabrica.Domain.Interfaces
{
    public interface ILinhaProducaoService
    {
        Task<LinhaProducao> AddLinhaProducao(AddLinhaProducaoCommand command);
    }


}