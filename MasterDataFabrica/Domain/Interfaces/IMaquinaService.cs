using System;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;

namespace MasterDataFabrica.Domain.Interfaces
{
    public interface IMaquinaService
    {
        Task<Maquina> AddMaquina(AddMaquinaCommand command);
        Task<Maquina> ChangeTipoMaquina(Guid id, Guid tipoMaquinaId);
        Task<bool> ChangeMaquinaState(Guid maquinaId, bool state);
    }


}