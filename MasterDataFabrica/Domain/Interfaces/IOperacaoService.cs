using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;

namespace MasterDataFabrica.Domain.Interfaces
{
    public interface IOperacaoService
    {
        Task<Operacao> AddOperacao(AddOperacaoCommand command);
    }


}