using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataFabrica.Domain.Interfaces
{
    public interface ITipoMaquinaService
    {
        Task<TipoMaquina> CreateTipoMaquina(AddTipoMaquinaCommand command);
        Task<List<Operacao>> GetOperacoesOfTipoMaquina(Guid id);
        Task<List<Maquina>> GetMaquinasOfTipoMaquina(Guid id);
        Task<TipoMaquina> ChangeOperacoes(Guid id, UpdateOperacoesTipoMaquinaCommand command);
    }


}