using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinhaProducaoController : ControllerBase
    {

        private readonly ILinhaProducaoService _linhaProducaoService;
        private readonly ILinhaProducaoRepository _linhaProducaoRepository;
        public LinhaProducaoController(ILinhaProducaoService linhaProducaoService, ILinhaProducaoRepository linhaProducaoRepository)
        {
            _linhaProducaoService = linhaProducaoService;
            _linhaProducaoRepository = linhaProducaoRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddLinhaProducaoCommand command)
        {
            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }
            LinhaProducao linhaProducao = await _linhaProducaoService.AddLinhaProducao(command);

            if (linhaProducao == null)
                return BadRequest("Cant Add");

            return Created("linhaproducao/" + linhaProducao.Id, new LinhaProducaoDTO
            {
                Id = linhaProducao.Id,
                Designacao = linhaProducao.Designacao
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<LinhaProducaoDTO>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest("Invalid Id");

            LinhaProducao linha = await _linhaProducaoRepository.GetLinhaProducaoById(id);

            if (linha is null)
                return NotFound("Não existe linha de produção com o identificador " + id);

            return new LinhaProducaoDTO()
            {
                Id = linha.Id,
                Designacao = linha.Designacao
            };
        }


        [HttpGet("{id}/maquinas")]
        public async Task<IEnumerable<MaquinaDTO>> GetMaquinasDeLinha([FromRoute]Guid id)
        {

            List<Maquina> maquinas = await _linhaProducaoRepository.GetMaquinasDeLinha(id);

            if (maquinas is null)
                return null;

            return maquinas.Select(x => new MaquinaDTO()
            {
                Id = x.Id,
                Marca = x.Marca,
                Modelo = x.Modelo,
                Active = x.Active
            });
        }

        [HttpGet]
        public async Task<IEnumerable<LinhaProducaoDTO>> GetAll()
        {

            List<LinhaProducao> linhas = await _linhaProducaoRepository.GetLinhasProducao();

            return linhas.Select(x => new LinhaProducaoDTO()
            {
                Id = x.Id,
                Designacao = x.Designacao
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            bool deleted = await _linhaProducaoRepository.DeleteLinhaProducao(id);

            if (!deleted)
                return BadRequest("Can't delete");

            return NoContent();
        }
    }
}