using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoMaquinaController : ControllerBase
    {

        private readonly ITipoMaquinaService _tipoMaquinaService;
        private readonly ITipoMaquinaRepository _tipoMaquinaRepository;
        public TipoMaquinaController(ITipoMaquinaService tipoMaquinaService, ITipoMaquinaRepository tipoMaquinaRepository)
        {
            _tipoMaquinaService = tipoMaquinaService;
            _tipoMaquinaRepository = tipoMaquinaRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddTipoMaquinaCommand command)
        {
            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }

            TipoMaquina tipoMaquina = await _tipoMaquinaService.CreateTipoMaquina(command);

            if (tipoMaquina == null)
                return BadRequest("Cant Add");

            return Created("tipomaquina/" + tipoMaquina.Id, new TipoMaquinaDTO { Id = tipoMaquina.Id, Nome = tipoMaquina.Nome });
        }

        [HttpPut("{id}/operacoes")]
        public async Task<IActionResult> Put([FromRoute]Guid id, [FromBody]UpdateOperacoesTipoMaquinaCommand command)
        {
            if (id == Guid.Empty)
                return BadRequest("Invalid Id");

            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }

            TipoMaquina tipoMaquina = await _tipoMaquinaService.ChangeOperacoes(id, command);

            if (tipoMaquina == null)
                return BadRequest("Cant Update");

            return Ok(new TipoMaquinaDTO { Id = tipoMaquina.Id, Nome = tipoMaquina.Nome });
        }


        [HttpGet]
        public async Task<IEnumerable<TipoMaquinaDTO>> GetAll()
        {

            List<TipoMaquina> tiposMaquinas = await _tipoMaquinaRepository.GetTiposMaquina();

            return tiposMaquinas.Select(x => new TipoMaquinaDTO()
            {
                Id = x.Id,
                Nome = x.Nome
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TipoMaquinaDTO>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest("Invalid Id");

            TipoMaquina tipoMaquina = await _tipoMaquinaRepository.GetTipoMaquinaById(id);

            if (tipoMaquina is null)
                return NotFound("Não existe tipo de máquina com o identificador " + id);

            return new TipoMaquinaDTO()
            {
                Id = tipoMaquina.Id,
                Nome = tipoMaquina.Nome
            };
        }

        //Consulta operacoes de Tipo de Maquina
        [HttpGet("{id}/operacoes")]
        public async Task<IEnumerable<OperacaoDTO>> GetOperacoes([FromRoute] Guid id)
        {
            List<Operacao> operacoes = await _tipoMaquinaService.GetOperacoesOfTipoMaquina(id);

            return operacoes.Select(x => new OperacaoDTO()
            {
                Id = x.Id,
                Descricao = x.Descricao,
                Duracao = x.Duracao
            });
        }

        //Consulta maquinas de Tipo de Maquina
        [HttpGet("{id}/maquinas")]
        public async Task<IEnumerable<MaquinaDTO>> GetMaquinas([FromRoute] Guid id)
        {
            List<Maquina> maquinas = await _tipoMaquinaService.GetMaquinasOfTipoMaquina(id);

            return maquinas.Select(x => new MaquinaDTO()
            {
                Id = x.Id,
                Marca = x.Marca,
                Modelo = x.Modelo,
                Active = x.Active
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            bool deleted = await _tipoMaquinaRepository.DeleteTipoMaquina(id);

            if (!deleted)
                return BadRequest("Can't delete");

            return NoContent();
        }
    }
}