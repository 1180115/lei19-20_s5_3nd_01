using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperacaoController : ControllerBase
    {

        private readonly IOperacaoService _operacaoService;
        private readonly IOperacaoRepository _operacaoRepository;
        public OperacaoController(IOperacaoService operacaoService, IOperacaoRepository operacaoRepository)
        {
            _operacaoService = operacaoService;
            _operacaoRepository = operacaoRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddOperacaoCommand command)
        {
            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }
            Operacao operacao = await _operacaoService.AddOperacao(command);

            if (operacao == null)
                return BadRequest("Cant Add");

            return Created("operacao/" + operacao.Id, operacao);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<OperacaoDTO>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest("Invalid Id");

            Operacao operacao = await _operacaoRepository.GetOperacaoById(id);

            if (operacao is null)
                return NotFound("Não existe operação com o identificador " + id);

            return new OperacaoDTO()
            {
                Id = operacao.Id,
                Descricao = operacao.Descricao,
                Duracao = operacao.Duracao
            };
        }
        [HttpGet]
        public async Task<IEnumerable<OperacaoDTO>> GetAll()
        {

            List<Operacao> operacao = await _operacaoRepository.GetOperacoes();

            return operacao.Select(x => new OperacaoDTO()
            {
                Id = x.Id,
                Descricao = x.Descricao,
                Duracao = x.Duracao
            });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            bool deleted = await _operacaoRepository.DeleteOperacao(id);

            if (!deleted)
                return BadRequest("Can't delete");

            return NoContent();
        }
    }
}