using MasterDataFabrica.Domain.Commands;
using MasterDataFabrica.Domain.Interfaces;
using MasterDataFabrica.Domain.Models;
using MasterDataFabrica.Infra.Interfaces;
using MasterDataFabrica.Shared.DTOs;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MasterDataFabrica.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaquinaController : ControllerBase
    {

        private readonly IMaquinaService _maquinaService;
        private readonly IMaquinaRepository _maquinaRepository;
        public MaquinaController(IMaquinaService maquinaService, IMaquinaRepository maquinaRepository)
        {
            _maquinaService = maquinaService;
            _maquinaRepository = maquinaRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddMaquinaCommand command)
        {
            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }
            Maquina maquina = await _maquinaService.AddMaquina(command);

            if (maquina == null)
                return BadRequest("Cant Add");

            return Created("maquina/" + maquina.Id, new MaquinaDTO
            {
                Id = maquina.Id,
                Marca = maquina.Marca,
                Modelo = maquina.Modelo
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MaquinaDTO>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest("Invalid Id");

            Maquina maquina = await _maquinaRepository.GetMaquinaById(id);

            if (maquina is null)
                return NotFound("Não existe máquina com o identificador " + id);

            return new MaquinaDTO()
            {
                Id = maquina.Id,
                Marca = maquina.Marca,
                Modelo = maquina.Modelo,
                tipoMaquina = new TipoMaquinaDTO
                {
                    Id = maquina.TipoMaquina.Id,
                    Nome = maquina.TipoMaquina.Nome
                },
                Active = maquina.Active
            };
        }
        [HttpGet]
        public async Task<IEnumerable<MaquinaDTO>> GetAll()
        {

            List<Maquina> maquinas = await _maquinaRepository.GetMaquinas();

            return maquinas.Select(x => new MaquinaDTO()
            {
                Id = x.Id,
                Marca = x.Marca,
                Modelo = x.Modelo,
                Active = x.Active
            });
        }

        [HttpGet("available")]
        public async Task<IEnumerable<MaquinaDTO>> GetMaquinasSemLinha()
        {

            List<Maquina> maquinas = await _maquinaRepository.GetMaquinasSemLinha();

            return maquinas.Select(x => new MaquinaDTO()
            {
                Id = x.Id,
                Marca = x.Marca,
                Modelo = x.Modelo
            });
        }

        [HttpPut("{id}/tipomaquina")]
        public async Task<IActionResult> ChangeTipoMaquina([FromRoute]Guid id, [FromBody]ChangeTipoMaquinaDeMaquinaCommand command)
        {
            if (id == Guid.Empty || !command.Validate())
                return BadRequest("Invalid Id");

            Maquina maquina = await _maquinaService.ChangeTipoMaquina(id, command.TipoMaquinaId);

            if (maquina == null)
                return BadRequest("Can't Add");

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            bool deleted = await _maquinaRepository.DeleteMaquina(id);

            if (!deleted)
                return BadRequest("Can't delete");

            return NoContent();
        }

        [HttpPatch("{id}/activate")]
        public async Task<IActionResult> ActivateMaquina([FromRoute]Guid id)
        {
            bool result = await _maquinaService.ChangeMaquinaState(id, true);

            if (result == false)
                return BadRequest();

            return NoContent();
        }

        [HttpPatch("{id}/deactivate")]
        public async Task<IActionResult> DeactivateMaquina([FromRoute]Guid id)
        {
            bool result = await _maquinaService.ChangeMaquinaState(id, false);

            if (result == false)
                return BadRequest();

            return NoContent();
        }
    }
}