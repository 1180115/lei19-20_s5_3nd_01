using System;

namespace MasterDataProducao.Shared.Interfaces
{
    public abstract class Entity
    {
        public Guid Id {get; private set;}
        public Entity()
        {
            Id = Guid.NewGuid();
        }
        protected Entity(Guid id)
        {
            Id = id;
        }
    }
}