using Microsoft.EntityFrameworkCore;
using MasterDataProducao.Domain;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MasterDataProducao.Domain.Models;

namespace MasterDataProducao.Shared.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }


        public DbSet<Produto> Produtos { get; set; }
        public DbSet<PlanoFabrico> PlanosFabrico { get; set; }
        public DbSet<PlanoFabricoOperacao> PlanosFabricoOperacao { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }

    }
}