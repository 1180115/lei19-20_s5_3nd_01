using System;
using System.Collections.Generic;

namespace MasterDataProducao.Shared.DTOs
{
    public class PlanoFabricoDTO
    {
        public Guid Id { get; set; }
        public List<OperacaoDTO> Operacoes { get; set; }
        public double TempoProducao { get; set; }
    }
}