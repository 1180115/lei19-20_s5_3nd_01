using System;

namespace MasterDataProducao.Shared.DTOs
{
    public class ProdutoDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public PlanoFabricoDTO PlanoFabrico { get; set; }
        public decimal Preco { get; set; }
        public double TempoFabrico { get; set; }
    }
}