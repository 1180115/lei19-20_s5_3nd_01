using System;
using MasterDataProducao.Shared.ValueObjects;

namespace MasterDataProducao.Shared.DTOs
{


    public class OperacaoDTO
    {
        public Guid Id;
        public string Descricao;
        public Duracao Duracao;
    }
}
