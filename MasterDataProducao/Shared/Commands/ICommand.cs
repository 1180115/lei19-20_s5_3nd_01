namespace MasterDataProducao.Shared.Command
{
    public interface ICommand
    {
        bool Validate();
    }
}