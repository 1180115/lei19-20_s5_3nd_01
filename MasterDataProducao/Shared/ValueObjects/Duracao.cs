namespace MasterDataProducao.Shared.ValueObjects
{

    public class Duracao
    {

        public double Tempo { get; private set; }

        public Duracao(double tempo)
        {
            Tempo = tempo;
        }
        protected Duracao() { }
    }

}