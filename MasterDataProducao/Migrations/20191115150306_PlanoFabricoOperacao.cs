﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataProducao.Migrations
{
    public partial class PlanoFabricoOperacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_PlanosFabrico_PlanoFabricoId",
                table: "Produtos");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_PlanoFabricoId",
                table: "Produtos");

            migrationBuilder.AddColumn<Guid>(
                name: "PlanoFabricoId1",
                table: "Produtos",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProdutoId",
                table: "PlanosFabrico",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "PlanosFabricoOperacao",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    PlanoFabricoId = table.Column<Guid>(nullable: false),
                    OperacaoId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanosFabricoOperacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PlanosFabricoOperacao_PlanosFabrico_PlanoFabricoId",
                        column: x => x.PlanoFabricoId,
                        principalTable: "PlanosFabrico",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_PlanoFabricoId1",
                table: "Produtos",
                column: "PlanoFabricoId1");

            migrationBuilder.CreateIndex(
                name: "IX_PlanosFabricoOperacao_PlanoFabricoId",
                table: "PlanosFabricoOperacao",
                column: "PlanoFabricoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_PlanosFabrico_PlanoFabricoId1",
                table: "Produtos",
                column: "PlanoFabricoId1",
                principalTable: "PlanosFabrico",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_PlanosFabrico_PlanoFabricoId1",
                table: "Produtos");

            migrationBuilder.DropTable(
                name: "PlanosFabricoOperacao");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_PlanoFabricoId1",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "PlanoFabricoId1",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "ProdutoId",
                table: "PlanosFabrico");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_PlanoFabricoId",
                table: "Produtos",
                column: "PlanoFabricoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_PlanosFabrico_PlanoFabricoId",
                table: "Produtos",
                column: "PlanoFabricoId",
                principalTable: "PlanosFabrico",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
