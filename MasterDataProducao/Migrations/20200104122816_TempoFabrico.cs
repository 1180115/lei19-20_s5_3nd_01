﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MasterDataProducao.Migrations
{
    public partial class TempoFabrico : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "TempoFabrico",
                table: "PlanosFabrico",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TempoFabrico",
                table: "PlanosFabrico");
        }
    }
}
