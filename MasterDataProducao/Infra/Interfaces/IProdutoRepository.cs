using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataProducao.Infra.Interfaces
{
    public interface IProdutoRepository
    {
        Task<List<Produto>> GetProdutos();
        Task<List<Produto>> GetProdutosWithPlano();
        Task<Produto> getProdutoById(Guid id);
        Task<Produto> AddProduto(Produto produto);
        Task DeleteProduto(Guid id);
    }
}