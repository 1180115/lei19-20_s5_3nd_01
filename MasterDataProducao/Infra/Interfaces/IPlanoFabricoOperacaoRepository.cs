using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MasterDataProducao.Infra.Interfaces
{
    public interface IPlanoFabricoOperacaoRepository
    {
        Task<PlanoFabricoOperacao> AddPlanoFabricoOperacao(PlanoFabricoOperacao planoFabricoOperacao);
        Task<List<Guid>> GetOperacoesDePlanoFabrico(Guid planoId);
    }
}