using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using  Microsoft.AspNetCore.Mvc;

namespace MasterDataProducao.Infra.Interfaces
{
    public interface IPlanoFabricoRepository
    {
        Task<List<PlanoFabrico>> getPlanosFabrico();
        Task<PlanoFabrico> getPlanoFabricoById(Guid id);
        Task<PlanoFabrico> addPlanoFabrico(PlanoFabrico planoFabrico);

        Task<PlanoFabrico> GetPlanoFabricoDeProduto(Guid produtoId);
    }
}