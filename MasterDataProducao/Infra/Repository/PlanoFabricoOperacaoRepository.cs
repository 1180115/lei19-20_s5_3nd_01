using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProducao.Infra.Repositories
{
    public class PlanoFabricoOperacaoRepository : IPlanoFabricoOperacaoRepository
    {
        private DataContext _dbContext;
        public PlanoFabricoOperacaoRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PlanoFabricoOperacao> AddPlanoFabricoOperacao(PlanoFabricoOperacao planoFabricoOperacao)
        {
            var result = _dbContext.PlanosFabricoOperacao.Add(planoFabricoOperacao);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<List<Guid>> GetOperacoesDePlanoFabrico(Guid planoId)
        {
            return await _dbContext.PlanosFabricoOperacao
                                                .Where(x => x.PlanoFabricoId == planoId)
                                                .OrderBy(x => x.Posicao)
                                                .Select(x => x.OperacaoId)
                                                .ToListAsync();
        }
    }
}