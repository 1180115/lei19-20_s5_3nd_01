using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Context;
using Microsoft.EntityFrameworkCore;

namespace MasterDataProducao.Infra.Repositories
{
    public class PlanoFabricoRepository : IPlanoFabricoRepository
    {
        private DataContext _dbContext;
        public PlanoFabricoRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<List<PlanoFabrico>> getPlanosFabrico()
        {
            return await _dbContext.PlanosFabrico.ToListAsync();
        }
        public async Task<PlanoFabrico> getPlanoFabricoById(Guid id)
        {
            return await _dbContext.PlanosFabrico.SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<PlanoFabrico> addPlanoFabrico(PlanoFabrico planoFabrico)
        {
            var result = _dbContext.PlanosFabrico.Add(planoFabrico);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<PlanoFabrico> GetPlanoFabricoDeProduto(Guid produtoId)
        {
            return await _dbContext.PlanosFabrico
                                    .Include(x => x.PlanosFabricoOperacao)
                                    .SingleOrDefaultAsync(x => x.ProdutoId == produtoId);
        }
    }
}