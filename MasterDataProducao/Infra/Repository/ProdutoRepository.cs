using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace MasterDataProducao.Infra.Repositories
{
    public class ProdutoRepository : IProdutoRepository
    {
        private DataContext _dbContext;
        public ProdutoRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<List<Produto>> GetProdutos()
        {
            return await _dbContext.Produtos
                                        .OrderBy(x => x.Nome)
                                        .ToListAsync();
        }

        public async Task<List<Produto>> GetProdutosWithPlano()
        {
            return await _dbContext.Produtos
                                        .Include(x => x.PlanoFabrico)
                                            .ThenInclude(x => x.PlanosFabricoOperacao)
                                        .OrderBy(x => x.Nome)
                                        .ToListAsync();
        }
        public async Task<Produto> getProdutoById(Guid id)
        {
            return await _dbContext.Produtos
                                    .Include(x => x.PlanoFabrico)
                                        .ThenInclude(x => x.PlanosFabricoOperacao)
                                    .SingleOrDefaultAsync(x => x.Id == id);
        }
        public async Task<Produto> AddProduto(Produto produto)
        {
            if (await _dbContext.Produtos.AnyAsync(x => x.Nome.Equals(produto.Nome)))
                return null;

            var result = _dbContext.Produtos.Add(produto);
            await _dbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task DeleteProduto(Guid id)
        {
            Produto produto = await _dbContext.Produtos.SingleOrDefaultAsync(x => x.Id == id);
            if (produto == null)
                return;
            _dbContext.Produtos.Remove(produto);

            await _dbContext.SaveChangesAsync();
        }
    }
}