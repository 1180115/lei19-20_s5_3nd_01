﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MasterDataProducao.Domain;
using MasterDataProducao.Infra;
using MasterDataProducao.Domain.Interfaces;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Shared.DTOs;

namespace MasterDataProducao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoService _produtoService;
        private readonly IProdutoRepository _produtoRepository;
        public ProdutoController(
            IProdutoService produtoService,
            IProdutoRepository produtoRepository)
        {
            _produtoService = produtoService;
            _produtoRepository = produtoRepository;
        }
        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<ProdutoDTO>> GetAll()
        {
            var listaProdutos = await _produtoService.GetProdutosApenasPlanoFabrico();

            return listaProdutos.Select(x => new ProdutoDTO
            {
                Id = x.Id,
                Nome = x.Nome,
                Preco = x.Preco,
                TempoFabrico = x.PlanoFabrico.TempoFabrico
            });
        }

        [HttpGet("tempofabrico")]
        public async Task<IEnumerable<ProdutoDTO>> GetAllPorTempoProducao()
        {
            var listaProdutos = await _produtoService.GetProdutosApenasPlanoFabrico();

            return listaProdutos
                .OrderBy(x => x.PlanoFabrico.TempoFabrico)
                .Select(x => new ProdutoDTO
                {
                    Id = x.Id,
                    Nome = x.Nome,
                    Preco = x.Preco,
                    TempoFabrico = x.PlanoFabrico.TempoFabrico
                });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProdutoDTO>> Get([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Invalid Id");
            }
            ProdutoDTO produtoDTO = await _produtoService.GetProdutoComPlanoFabrico(id); ;

            if (produtoDTO is null)
            {
                return NotFound("Não existe Produto com o id " + id);
            }

            return produtoDTO;
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddProdutoCommand command)
        {
            if (!command.Validate())
            {
                return BadRequest("Invalid Command");
            }
            Produto produto = await _produtoService.AddProduto(command);

            if (produto == null)
                return BadRequest("Cant Add");

            return Created("produto/" + produto.Id, new ProdutoDTO
            {
                Id = produto.Id,
                Nome = produto.Nome,
                Preco = produto.Preco
            });
        }

        [HttpGet("{id}/planofabrico")]
        public async Task<ActionResult<PlanoFabricoDTO>> GetPlanoFabrico([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest("Invalid Id");
            }
            Produto produto = await _produtoRepository.getProdutoById(id);

            if (produto is null)
            {
                return NotFound("Não existe Produto com o id " + id);
            }

            return new PlanoFabricoDTO { Id = produto.PlanoFabricoId };
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            await _produtoRepository.DeleteProduto(id);

            return NoContent();
        }
    }
}
