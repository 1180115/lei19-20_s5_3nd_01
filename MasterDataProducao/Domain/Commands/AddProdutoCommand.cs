using System;
using System.Collections.Generic;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Command;

namespace MasterDataProducao.Domain.Commands
{
    public class AddProdutoCommand : ICommand
    {
        public string Nome;
        public List<Slot> operacoesId;
        public decimal Preco;

        public bool Validate()
        {
            if (string.IsNullOrWhiteSpace(Nome) ||
                operacoesId.Count == 0)
            {
                return false;
            }
            return true;
        }
    }

    public class Slot
    {
        public Guid operacaoId;
        public int Posicao;
    }
}