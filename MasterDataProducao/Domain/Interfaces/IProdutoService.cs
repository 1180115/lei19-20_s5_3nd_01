using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Shared.DTOs;

namespace MasterDataProducao.Domain.Interfaces
{
    public interface IProdutoService
    {
        Task<Produto> AddProduto(AddProdutoCommand command);
        Task<ProdutoDTO> GetProdutoComPlanoFabrico(Guid produtoId);
        Task<List<Produto>> GetProdutosApenasPlanoFabrico();
    }
}