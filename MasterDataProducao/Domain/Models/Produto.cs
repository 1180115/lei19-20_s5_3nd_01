using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Interfaces;

namespace MasterDataProducao.Domain.Models
{
    public class Produto : Entity
    {
        public string Nome { get; private set; }

        public PlanoFabrico PlanoFabrico { get; set; }

        public Guid PlanoFabricoId { get; private set; }

        public decimal Preco {get; private set;}

        public Produto(string nome, decimal preco)
        {
            Nome = nome;
            Preco = preco;
        }
        protected Produto()
        { }
    }
}