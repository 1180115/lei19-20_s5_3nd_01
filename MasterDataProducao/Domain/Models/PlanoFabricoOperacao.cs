using System;
using System.Collections.Generic;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Interfaces;

namespace MasterDataProducao.Domain.Models
{
    public class PlanoFabricoOperacao : Entity
    {
        public Guid PlanoFabricoId { get; private set; }
        public PlanoFabrico PlanoFabrico { get; private set; }
        public int Posicao { get; private set; }


        public Guid OperacaoId { get; private set; }

        //TODO Vazio por agora, alterar na próxima iteração
        public PlanoFabricoOperacao(PlanoFabrico planoFabrico, Guid operacaoId, int posicao)
        {
            PlanoFabrico = planoFabrico;
            OperacaoId = operacaoId;
            Posicao = posicao;
        }

        protected PlanoFabricoOperacao()
        {
        }

    }
}