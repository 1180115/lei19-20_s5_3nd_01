using System;
using System.Collections.Generic;
using MasterDataProducao.Shared;
using MasterDataProducao.Shared.Interfaces;

namespace MasterDataProducao.Domain.Models
{
    public class PlanoFabrico : Entity
    {
        public Guid ProdutoId { get; private set; }
        public List<PlanoFabricoOperacao> PlanosFabricoOperacao { get; private set; }

        public double TempoFabrico { get; private set; }
        //TODO Vazio por agora, alterar na próxima iteração
        public PlanoFabrico(Guid produtoId, double tempoFabrico)
        {
            ProdutoId = produtoId;
            TempoFabrico = tempoFabrico;
        }

        public void AlterarTempoFabrico(double tempo)
        {
            TempoFabrico = tempo;
        }
    }
}