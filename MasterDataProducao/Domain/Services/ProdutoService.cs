using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using MasterDataProducao.Domain.Commands;
using MasterDataProducao.Domain.Interfaces;
using MasterDataProducao.Domain.Models;
using MasterDataProducao.Infra;
using MasterDataProducao.Infra.Interfaces;
using MasterDataProducao.Shared.DTOs;

namespace MasterDataProducao.Domain.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;
        private readonly IPlanoFabricoOperacaoRepository _planoFabricoOperacaoRepository;
        private readonly IPlanoFabricoRepository _planoFabricoRepository;

        public ProdutoService(
                IProdutoRepository produtoRepository,
                IPlanoFabricoOperacaoRepository planoFabricoOperacaoRepository,
                IPlanoFabricoRepository planoFabricoRepository)
        {
            _produtoRepository = produtoRepository;
            _planoFabricoOperacaoRepository = planoFabricoOperacaoRepository;
            _planoFabricoRepository = planoFabricoRepository;
        }
        public async Task<Produto> AddProduto(AddProdutoCommand command)
        {
            string URL = "https://masterdatafabrica.azurewebsites.net/api/operacao";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Add("access-control-allow-methods", "[GET]");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            double tempoProducao = 0;
            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait dhere until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<OperacaoDTO>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
                foreach (var d in command.operacoesId)
                {
                    var item = dataObjects.SingleOrDefault(x => x.Id == d.operacaoId);
                    if (item == null)
                    {
                        return null;
                    }
                    else
                    {
                        tempoProducao += item.Duracao.Tempo;
                    }
                }
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();

            Produto produto = new Produto(command.Nome, command.Preco);
            Produto produtoResult = await _produtoRepository.AddProduto(produto);

            if (produtoResult is null)
                return null;

            PlanoFabrico planoFabrico = new PlanoFabrico(produto.Id, tempoProducao);

            foreach (var i in command.operacoesId)
            {
                var result = await _planoFabricoOperacaoRepository.AddPlanoFabricoOperacao(new PlanoFabricoOperacao(planoFabrico, i.operacaoId, i.Posicao));

                if (result is null)
                {
                    return null;
                }
            }


            return produtoResult;
        }

        public async Task<ProdutoDTO> GetProdutoComPlanoFabrico(Guid produtoId)
        {
            Produto produto = await _produtoRepository.getProdutoById(produtoId);

            PlanoFabrico planoFabrico = await _planoFabricoRepository.GetPlanoFabricoDeProduto(produtoId);

            List<Guid> lista = await _planoFabricoOperacaoRepository.GetOperacoesDePlanoFabrico(planoFabrico.Id);

            string URL = "https://masterdatafabrica.azurewebsites.net/api/operacao";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Add("access-control-allow-methods", "[GET]");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));


            List<OperacaoDTO> operacoes = new List<OperacaoDTO>();

            // List data response.
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body.
                var dataObjects = response.Content.ReadAsAsync<IEnumerable<OperacaoDTO>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll

                foreach (var i in lista)
                {
                    OperacaoDTO op = dataObjects.SingleOrDefault(x => x.Id == i);

                    if (op is null)
                        return null;

                    operacoes.Add(op);
                }
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
            client.Dispose();

            return new ProdutoDTO()
            {
                Id = produto.Id,
                Nome = produto.Nome,
                PlanoFabrico = new PlanoFabricoDTO()
                {
                    Id = planoFabrico.Id,
                    Operacoes = operacoes,
                    TempoProducao = planoFabrico.TempoFabrico
                },
                Preco = produto.Preco
            };
        }

        public async Task<List<Produto>> GetProdutosApenasPlanoFabrico()
        {
            List<Produto> produtos = await _produtoRepository.GetProdutos();
            List<PlanoFabrico> planos = await _planoFabricoRepository.getPlanosFabrico();

            foreach (var p in produtos)
            {
                PlanoFabrico plano = planos.SingleOrDefault(x => x.ProdutoId == p.Id);
                if (plano != null)
                    p.PlanoFabrico = plano;
            }

            return produtos;
        }
    }
}